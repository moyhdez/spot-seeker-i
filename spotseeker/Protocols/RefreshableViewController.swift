//
//  RefreshableViewController.swift
//  spotseeker
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

protocol RefreshableViewController {
    var refreshControl: UIRefreshControl! { get set }
}
