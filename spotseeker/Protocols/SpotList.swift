//
//  SpotList.swift
//  spotseeker
//
//  Created by Moy Hdez on 18/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

protocol SpotList: class {
    var tableView: UITableView! { get set }
    var refreshControl: UIRefreshControl! { get set }
    var locationManager: CLLocationManager { get set }
    var userLocation: CLLocationCoordinate2D? { get set }
    var locationSent: CLLocationCoordinate2D? { get set }
    var pagination: Pagination? { get set }
    var isFirstTime: Bool { get set }
    var firstTimeLocationUpdate: (()->())? { get set }
    var spots: [Spot] { get set }
}
