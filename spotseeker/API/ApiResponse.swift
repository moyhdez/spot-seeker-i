//
//  ApiResponse.swift
//  SafoDriver
//
//  Created by Moy Hdez on 18/10/17.
//  Copyright © 2017 Tejuino. All rights reserved.
//

import Foundation

struct ApiResponse {
    var data: JSON = [:]
    var error: String?
    var code: Int?
    
    init(json: JSON) {
        code = json["code"] as? Int
        if let dataJson = json["data"] as? JSON {
            data = dataJson
        } else {
            data = json
        }
        
        error = data["error"] as? String ?? NSLocalizedString("We have encountered an internal error. Please try again.", comment: "")
    }
    
    init(error: String) {
        self.error = error
    }
}
