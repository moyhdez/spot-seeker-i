//
//  ApiManager.swift
//  SafoDriver
//
//  Created by Moy Hdez on 18/10/17.
//  Copyright © 2017 Tejuino. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRequest {
    case GetLoggedUser(vc: UIViewController?, params: JSON)
    case Register(vc: UIViewController?, params: JSON)
    case UpdateProfile(vc: UIViewController?, params: JSON)
    case LoginByEmail(vc: UIViewController?, params: JSON)
    case LoginFacebook(vc: UIViewController?, params: JSON)
    case RecoverPassword(vc: UIViewController?, params: JSON)
    case UpdatePassword(vc: UIViewController?, params: JSON)
    case UpdateFCMToken(params: JSON)
    case Logout(vc: UIViewController?, params: JSON)
    
    case Notifications(vc: UIViewController?, page: Int)
    
    case UserSpots(vc: UIViewController?, params: JSON, page: Int)
    case UserFavorites(vc: UIViewController?, params: JSON, page: Int)
    case SpotsFilter(vc: UIViewController?, params: JSON, page: Int)
    case MapSpots(vc: UIViewController?, params: JSON)
    case SpotComments(vc: UIViewController?, id: Int, page: Int)
    case Spot(vc: UIViewController?, id: Int)
    case DeleteSpot(vc: UIViewController?, id: Int)
    case CheckIn(vc: UIViewController?, id: Int)
    case Followers(vc: UIViewController?, user: User, page: Int)
    case Following(vc: UIViewController?, user: User, page: Int)
    
    case CreateSpot(vc: UIViewController?, params: JSON)
    case UpdateSpot(vc: UIViewController?, params: JSON, spotID: Int)
    case AddToFavorites(vc: UIViewController?, id: Int)
    case AddComment(vc: UIViewController?, id: Int, params: JSON)
    case DeleteComment(vc: UIViewController?, id: Int)
    
    case Categories(vc: UIViewController?)
    
    case GetUser(vc: UIViewController?, id: Int)
    case GetEvents(vc: UIViewController?)
    case GetPromos(vc: UIViewController?)
    case FollowUser(vc: UIViewController?, userID: Int)
    case GetFeed(vc: UIViewController?, page: Int)
    case GetFeedPost(vc: UIViewController?, id: Int)
    case Search(vc: UIViewController?, search: String)
    
    var method : Alamofire.HTTPMethod {
        switch self {
        case .GetLoggedUser:
            return .get
        case .UserSpots:
            return .get
        case .Notifications:
            return .get
        case .MapSpots:
            return .get
        case .Spot:
            return .get
        case .SpotsFilter:
            return .get
        case .UserFavorites:
            return .get
        case .Followers:
            return .get
        case .Following:
            return .get
        case .SpotComments:
            return .get
        case .DeleteComment:
            return .delete
        case .Categories:
            return .get
        case .DeleteSpot:
            return .delete
        case .GetUser:
            return .get
        case .GetEvents:
            return .get
        case .GetPromos:
            return .get
        case .FollowUser:
            return .get
        case .UpdateSpot:
            return .put
        case .GetFeed:
            return .get
        case .GetFeedPost:
            return .get
        case .Search:
            return .get
        case .CheckIn:
            return .get
        default:
            return .post
        }
    }

    var url: String {
        switch self {
        case .Register:
            return "api/account/register"
        case .GetLoggedUser:
            return "api/account/profile"
        case .UpdateProfile:
            return "api/account/update"
        case .LoginByEmail:
            return "api/login"
        case .LoginFacebook:
            return "api/login/facebook"
        case .Logout:
            return "api/logout"
        case .Notifications(_, let page):
            return "api/notifications?page=\(page)"
        case .UserSpots(_, _, let page):
            return "api/account/spots?page=\(page)"
        case .UserFavorites(_, _, let page):
            return "api/account/favorites?page=\(page)"
        case .Followers(_, let user, let page):
            return "api/users/\(user.id)/followers?page=\(page)"
        case .Following(_, let user, let page):
            return "api/users/\(user.id)/following?page=\(page)"
        case .SpotComments(_, let id, let page):
            return "api/spots/\(id)/comments?page=\(page)"
        case .Spot(_, let id):
            return "api/spots/\(id)"
        case .UpdateSpot(_, _, let id):
            return "api/spots/\(id)"
        case .CreateSpot:
            return "api/spots"
        case .AddToFavorites(_, let id):
            return "api/spots/\(id)/favorites"
        case .DeleteSpot(_, let id):
            return "api/spots/\(id)"
        case .AddComment(_, let id, _):
            return "api/spots/\(id)/comments"
        case .DeleteComment(_, let id):
            return "api/comment/\(id)"
        case .CheckIn(_, let id):
            return "api/spots/\(id)/checkIn"
        case .RecoverPassword:
            return "api/account/recover"
        case .UpdatePassword:
            return "api/account/updatePassword"
        case .UpdateFCMToken:
            return "api/account/updateFCMToken"
        case .MapSpots:
            return "api/spots/map"
        case .SpotsFilter(_, _, let page):
            return "api/spots/filter?page=\(page)"
        case .Categories:
            return "api/categories"
        case .GetUser(_, let id):
            return "api/users/\(id)"
        case .FollowUser(_, let id):
            return "api/users/\(id)/follow"
        case .GetEvents:
            return "api/events"
        case .GetPromos:
            return "api/promotions"
        case .GetFeed(_, let page):
            return "api/articles?page=\(page)"
        case .GetFeedPost(_, let id):
            return "api/articles/\(id)"
        case .Search:
            return "api/search"
        }
    }

    var headers: APIClient.Header {
        switch self {
        default:
            return .Default
        }
    }

    var vc: UIViewController? {
        switch self {
        case .Register(let vc, _):
            return vc
        case .GetLoggedUser(let vc, _):
            return vc
        case .UpdateProfile(let vc, _):
            return vc
        case .LoginByEmail(let vc, _):
            return vc
        case .LoginFacebook(let vc, _):
            return vc
        case .Logout(let vc, _):
            return vc
        case .Notifications(let vc, _):
            return vc
        case .Spot(let vc, _):
            return vc
        case .SpotsFilter(let vc, _, _):
            return vc
        case .MapSpots(let vc, _):
            return vc
        case .UserSpots(let vc, _, _):
            return vc
        case .Followers(let vc, _, _):
            return vc
        case .Following(let vc, _, _):
            return vc
        case .UserFavorites(let vc, _, _):
            return vc
        case .CreateSpot(let vc, _):
            return vc
        case .UpdateSpot(let vc, _, _):
            return vc
        case .DeleteSpot(let vc, _):
            return vc
        case .AddToFavorites(let vc, _):
            return vc
        case .AddComment(let vc, _, _):
            return vc
        case .DeleteComment(let vc, _):
            return vc
        case .CheckIn(let vc, _):
            return vc
        case .RecoverPassword(let vc, _):
            return vc
        case .UpdatePassword(let vc, _):
            return vc
        case .Categories(let vc):
            return vc
        case .GetEvents(let vc):
            return vc
        case .GetPromos(let vc):
            return vc
        case .GetUser(let vc, _):
            return vc
        case .FollowUser(let vc, _):
            return vc
        case .GetFeed(let vc, _):
            return vc
        case .GetFeedPost(let vc, _):
            return vc
        case .Search(let vc, _):
            return vc
        default:
            return nil
        }
    }

    var params: [String: Any]? {
        switch self {
        case .UpdateProfile(_, let params):
            return params
        case .LoginByEmail(_, let params):
            return params
        case .LoginFacebook(_, let params):
            return params
        case .Logout(_, let params):
            return params
        case .GetLoggedUser(_, let params):
            return params
        case .Register(_, let params):
            return params
        case .RecoverPassword(_, let params):
            return params
        case .CreateSpot(_, let params):
            return params
        case .UpdateSpot(_, let params, _):
            return params
        case .AddComment(_, _, let params):
            return params
        case .UpdatePassword(_, let params):
            return params
        case .UpdateFCMToken(let params):
            return params
        case .SpotsFilter(_, let params, _):
            return params
        case .UserSpots(_, var params, let page):
            params.updateValue("page", forKey: "\(page)")
            return params
        case .UserFavorites(_, var params, let page):
            params.updateValue("page", forKey: "\(page)")
            return params
        case .MapSpots(_, let params):
            return params
        case .Search(_, let search):
            return ["search": search]
        default:
            return nil
        }
    }
    
    var showLoader: Bool {
        switch self {
        case .MapSpots:
            return false
        default:
            return true
        }
    }
    
    var openLogin: Bool {
        switch self {
        case .LoginByEmail:
            return false
        default:
            return true
        }
    }

    var showErrorAlert: Bool {
        switch self {
        default:
            return true
        }
    }
}
