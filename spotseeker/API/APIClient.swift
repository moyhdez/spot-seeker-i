//
//  APIClient.swift
//  designado
//
//  Created by Moy Hdez on 25/04/17.
//  Copyright © 2017 CJAPPS. All rights reserved.
//

import UIKit
import Alamofire

typealias JSON = [String:Any]

class APIClient {
    
    enum Header {
        case Default
        case Empty
        
        var header: HTTPHeaders? {
            switch self {
            case .Default:
                guard let accessToken = UserDefaults.standard.value(forKey: K.Defaults.AccessToken) else {
                    return ["Accept":"application/json","X-localization": Locale.current.languageCode ?? "en"]
                }
                return ["Accept":"application/json","Authorization":"Bearer \(accessToken)", "X-localization": Locale.current.languageCode ?? "en"]
            case .Empty:
                return  ["Accept":"application/json","X-localization": Locale.current.languageCode ?? "en"]
            }
        }
    }
    
    private func multipartRequest(request: ApiRequest, completion: @escaping (_ response: ApiResponse, _ success: Bool) -> Void) {
        var showLoader = request.showLoader
        if let vc = request.vc as? RefreshableViewController, vc.refreshControl.isRefreshing {
            showLoader = false
        } else if let vc = request.vc as? UITableViewController, vc.refreshControl?.isRefreshing ?? false {
            showLoader = false
        }
        if let view = request.vc?.view, showLoader {
            LilithProgressHUD.show(view)
        }
        
        Alamofire.upload(multipartFormData: { (form) in
            guard let params = request.params else { return }
            for (key, value) in params {
                if let image = value as? UIImage, let data = image.jpegData(compressionQuality: 0.3) {
                    form.append(data, withName: key, fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                } else if let images = value as? [UIImage] {
                    for image in images {
                        if let data = image.jpegData(compressionQuality: 0.3) {
                            form.append(data, withName: "\(key)[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
                        }
                    }
                } else if let data = (value as AnyObject).data(using: String.Encoding.utf8.rawValue) {
                    form.append(data, withName: key)
                }
            }
        }, to: K.Server.BaseURL+request.url, method: request.method, headers: request.headers.header, encodingCompletion: { result in
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    self.handle(response, request: request, completion: completion)
                })
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    
    func request(request: ApiRequest, completion: @escaping (_ response: ApiResponse, _ success: Bool) -> Void) {
        var showLoader = request.showLoader
        if let vc = request.vc as? RefreshableViewController, vc.refreshControl?.isRefreshing ?? false {
            showLoader = false
        } else if let vc = request.vc as? UITableViewController, vc.refreshControl?.isRefreshing ?? false {
            showLoader = false
        }
        if let view = request.vc?.view, showLoader {
            LilithProgressHUD.show(view)
        }
        print(K.Server.BaseURL+request.url)
        Alamofire.request(K.Server.BaseURL+request.url, method: request.method, parameters: request.params, encoding: URLEncoding.default, headers: request.headers.header).responseJSON { (response) -> Void in
            self.handle(response, request: request, completion: completion)
        }
    }
    
    private func handle(_ response: DataResponse<Any>, request: ApiRequest, completion: @escaping (_ response: ApiResponse, _ success: Bool) -> Void) {
        if let view = request.vc?.view {
            LilithProgressHUD.hide(view)
        }
        if let vc = request.vc as? RefreshableViewController, vc.refreshControl.isRefreshing {
            vc.refreshControl.endRefreshing()
        }else if let vc = request.vc as? UITableViewController, vc.refreshControl?.isRefreshing ?? false {
            vc.refreshControl?.endRefreshing()
        }
        
        var apiResponse = ApiResponse(error: NSLocalizedString("We have encountered an internal error. Please try again.", comment: ""))
        
        if let json = response.value as? JSON {
            apiResponse = ApiResponse(json: json)
        }
        
        let statusCode = response.response?.statusCode ?? 500
        let success = (200...300).contains(statusCode)
        completion(apiResponse, success)
        
        if statusCode == 401 && request.openLogin {
            let vc = LoginVC.instanceAsNc()
            request.vc?.present(vc, animated: true, completion: nil)
        }
        
        if request.showErrorAlert, !success {
            self.handle(apiResponse, for: request.vc)
        }
    }
    
    func handle(_ response: ApiResponse, for vc: UIViewController? ) {
        if let error = response.error, let vc = vc {
            AlertsController.showAlert(delegate: vc, title: nil, message: error, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
}

