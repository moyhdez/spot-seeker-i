//
//  String+Ext.swift
//  Homing
//
//  Created by Miguel Jimenez on 22/01/18.
//  Copyright © 2018 oOMovil. All rights reserved.
//

import UIKit

extension String {
    
    func formatAsDate(fromFormat: String = "yyyy-MM-dd HH:mm:ss", toFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "es_MX")
        formatter.dateFormat = fromFormat
        let date = formatter.date(from: self)
        formatter.dateFormat = toFormat
        
        if let formatDate = date {
            return formatter.string(from: formatDate)
        }
        
        print("No fue posible dar formato a tu fecha")
        return self
    }
    
    func getDate(fromFormat: String) -> Date? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "es_MX")
        formatter.dateFormat = fromFormat
        return formatter.date(from: self)
    }
    
    func removeCommas() -> String {
        return self.replacingOccurrences(of: ",", with: "")
    }
    
    func toInt() -> Int {
        return (self as NSString).integerValue
    }
    
    func toNSNumber() -> NSNumber {
        return NSNumber(value: self.toInt())
    }
    
    func toFloat() -> Float {
        return (self as NSString).floatValue
    }
    
    func toDouble() -> Double {
        return (self as NSString).doubleValue
    }
    
    func getAttributedString( with atrributes :[NSAttributedString.Key:AnyObject]? = nil) -> NSAttributedString {
        if atrributes != nil {
            return NSAttributedString(string: self, attributes: atrributes)
        } else {
            return NSAttributedString(string: self)
        }
    }
    
    static func applyAttributesToTarget(string: String,target: String,color: UIColor,font: UIFont) -> NSMutableAttributedString{
        
        let atributos: [NSAttributedString.Key:AnyObject] = [NSAttributedString.Key.foregroundColor : color, NSAttributedString.Key.font : font]
        let rangoStringConColor = NSString(string: string).range(of: target)
        let stringConAtributos = NSMutableAttributedString(string: string)
        stringConAtributos.addAttributes(atributos, range: rangoStringConColor)
        return stringConAtributos
    }
    
    func checkRegex(pattern: String) -> Bool {
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        return regex?.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
    }
    
    func isValidEmail() -> Bool {
        
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        return result
    }
}
