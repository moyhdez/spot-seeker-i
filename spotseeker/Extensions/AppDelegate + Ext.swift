//
//  AppDelegate-Extension.swift
//  spotseeker
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import Firebase
import UserNotifications
//import GLNotificationBar

extension AppDelegate {
    
    // AppDelegate SharedInstance
    static func sharedInstance() -> AppDelegate{
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    static func checkUser(_ delegate: UIViewController) -> Bool {
        if User.getFromDefaults() == nil {
            let vc = LoginVC.instanceAsNc()
            delegate.present(vc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func switchRootViewController(vc: UIViewController, animated: Bool, completion: (()-> Void)? = nil){
        if animated{
            UIView.transition(with: self.window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                let oldState: Bool = UIView.areAnimationsEnabled
                UIView.setAnimationsEnabled(false)
                self.window?.rootViewController = vc
                UIView.setAnimationsEnabled(oldState)
            }, completion: { (finished: Bool) in
                if completion != nil{
                    completion!()
                }
            })
        }else{
            self.window?.rootViewController = vc
        }
    }
    
    static func topViewController(controller: UIViewController?) -> UIViewController? {
        var topController = controller
        while (topController?.presentedViewController != nil) {
            topController = topController?.presentedViewController
        }
        return topController
    }
    
    static func topMostController(vc: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        let controller = AppDelegate.topViewController(controller: vc)
        if let navigationController = controller as? UINavigationController {
            return topMostController(vc: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topMostController(vc: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topMostController(vc: presented)
        }
        return controller
    }
    
    static func logout(vc: UIViewController? = nil) {
        var params: JSON = [:]
        if let id = AppDelegate.getDeviceId() {
            params.updateValue(id, forKey: "device_id")
        }
        APIClient().request(request: .Logout(vc: vc, params: params)) { (_, _) in
            User.destroy()
            AppDelegate.removeDeviceId()
            AppDelegate.sharedInstance().switchRootViewController(vc: MainTabBarController(), animated: true)
        }
    }
    
    static func open(_ url: URL) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    static func openMap(_ delegate: UIViewController, with coordinates: CLLocationCoordinate2D) {
        let menuView = UIAlertController(title: "Open with", message: "", preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            menuView.dismiss(animated: true, completion: nil)
        }
        
        let googleMapsUrl = URL(fileURLWithPath: "https://www.google.com/maps/search/?api=1")
        let wazeUrl = URL(fileURLWithPath: "https://waze.com/ul")
        let appleMapsUrl = URL(fileURLWithPath: "http://maps.apple.com")
        
        let customGoogleMapsUrl = "https://www.google.com/maps/search/?api=1&query=\(coordinates.latitude),\(coordinates.longitude)"
        let customWazeUrl = "https://waze.com/ul?ll=\(coordinates.latitude),\(coordinates.longitude)"
        let customAppleMapsUrl = "http://maps.apple.com/?daddr=\(coordinates.latitude),\(coordinates.longitude)"
        // eval if the app google maps is installed
        if UIApplication.shared.canOpenURL(googleMapsUrl) {
            let action = UIAlertAction(title: "Google Maps", style: .default) { (action) in
                if let url = URL(string: customGoogleMapsUrl) {
                    AppDelegate.open(url)
                }
            }
            menuView.addAction(action)
        }
        // eval if waze is installed
        if UIApplication.shared.canOpenURL(wazeUrl) {
            let action = UIAlertAction(title: "Waze", style: .default) { (action) in
                if let url = URL(string: customWazeUrl) {
                    AppDelegate.open(url)
                }
            }
            menuView.addAction(action)
        }
        // eval if apple maps is installed
        if UIApplication.shared.canOpenURL(appleMapsUrl) {
            let action = UIAlertAction(title: "Mapas", style: .default) { (action) in
                if let url = URL(string: customAppleMapsUrl) {
                    AppDelegate.open(url)
                }
            }
            menuView.addAction(action)
        }
        
        menuView.addAction(cancelAction)
        delegate.present(menuView, animated: true)
    }
    
}

