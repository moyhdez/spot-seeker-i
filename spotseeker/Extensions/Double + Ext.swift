//
//  Double + Ext.swift
//  spotseeker
//
//  Created by Moy Hdez on 18/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

extension Double {
    
    func formatAsCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let formattedTipAmount = formatter.string(from: self as NSNumber) {
            return formattedTipAmount
        }
        
        return ""
    }
}
