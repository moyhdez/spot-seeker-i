//
//  AppDelegate+Firebase.swift
//  SideCar
//
//  Created by Miguel Jimenez on 17/05/18.
//  Copyright © 2018 oOMovil. All rights reserved.
//

import Foundation
import Firebase
import UserNotifications

//MARK: PUSH NOTIFICATION
extension AppDelegate {
    
    static func getDeviceId() -> Int? {
        let deviceID = UserDefaults.standard.integer(forKey: "device_id")
        return deviceID > 0 ? deviceID : nil
    }
    
    static func setDeviceId(deviceId: Int) {
        UserDefaults.standard.set(deviceId, forKey: "device_id")
    }
    
    static func removeDeviceId() {
        UserDefaults.standard.removeObject(forKey: "device_id")
    }
    
    func firebaseConfigure() {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
    
    func registerForPushNotifications() {
        let application = UIApplication.shared
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    static func setBadgeNumber(_ num: Int) {
        let updatedBadgeNumber = num
        if (updatedBadgeNumber > -1) {
            UIApplication.shared.applicationIconBadgeNumber = updatedBadgeNumber
        }
    }
    
    func handle(_ userInfo: [AnyHashable: Any]) {
        if let str = userInfo["spot_id"] as? String, let spotId = Int(str) {
            self.getSpot(id: spotId)
        } else if let str = userInfo["user_id"] as? String, let userId = Int(str) {
            self.getUser(id: userId)
        } else if let str = userInfo["article_id"] as? String, let postId = Int(str) {
            self.getFeedPost(id: postId)
        } else {
            if let _ = User.getFromDefaults() {
                AppDelegate.topMostController()?.navigationController?.pushViewController(NotificationsVC.instance(), animated: true)
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Fail To register remote notification: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("notification received-- didReceiveRemoteNotification-fetchCompletionHandler")
        handle(userInfo)
        completionHandler(.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("notification received-- didReceiveRemoteNotification")
        handle(userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)
        
        if let token = Messaging.messaging().fcmToken {
            print("TOKEN: \(token)")
            registerDevice(token: token)
        }
    }
}

//MARK: - USERNOTIFICATIONS
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound,.badge])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        handle(response.notification.request.content.userInfo)
        completionHandler()
    }
}

//MARK: - MESSAGING
extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("notification received - messaging didReceive")
        handle(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("TOKEN REFRESH: \(fcmToken)")
        registerDevice(token: fcmToken)
    }
    
    func registerDevice(token: String) {
        if User.getFromDefaults() == nil {
            return
        }
        var parameters: JSON = ["os": "ios",
                                "token": token]
        if let deviceID = AppDelegate.getDeviceId() {
            parameters.updateValue("\(deviceID)", forKey: "device_id")
            print("old device_id(UPDATE FCM): \(deviceID)")
        }
        
        
        APIClient().request(request: .UpdateFCMToken(params: parameters)) { (response, success) in
            if success {
                guard let deviceId = response.data["device_id"] as? Int else { return }
                print("new device_id(UPDATE FCM): \(deviceId)")
                AppDelegate.setDeviceId(deviceId: deviceId)
            }
        }
    }
}


//MARK: - REQUESTS
extension AppDelegate {
    
    func getUser(id: Int) {
        let topVC = AppDelegate.topMostController()
        APIClient().request(request: .GetUser(vc: topVC, id: id)) { (response, success) in
            if success {
                guard let userDic = response.data["user"] as? JSON
                    , let user = try? User(userDic) else { topVC?.showServerBadErrorResponse(); return }
                let vc = ProfileVC.instance()
                vc.user = user
                topVC?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getSpot(id: Int) {
        let topVC = AppDelegate.topMostController()
        APIClient().request(request: .Spot(vc: topVC, id: id)) {(response, success) in
            if success {
                guard let spotJSON = response.data["spot"] as? JSON, let spot = try? Spot(spotJSON) else { topVC?.showServerBadErrorResponse(); return }
                let vc = DetailSpotVC.instance()
                vc.spot = spot
                topVC?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getFeedPost(id: Int) {
        let topVC = AppDelegate.topMostController()
        APIClient().request(request: .GetFeedPost(vc: topVC, id: id)) {(response, success) in
            if success {
                guard let post = try? FeedPost(response.data) else { topVC?.showServerBadErrorResponse(); return  }
                let vc = FeedDetailVC.instance()
                vc.post = post
                topVC?.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
