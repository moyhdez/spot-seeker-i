//
//  UIViewController+Extensions.swift
//  spotseeker
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class func instance() -> Self {
        let storyboardName = String(describing: self)
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.initialViewController()
    }
    
    class func instanceAsNc() -> UINavigationController {
        let storyboardName = String(describing: self)
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let vc = storyboard.initialViewController()
        let nc = UINavigationController(rootViewController: vc)
        return nc
    }
    
    func removeBackTitle() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItem.Style.plain, target: self, action: nil)
    }
    
    func goToProfile(of user: User) {
        let vc = ProfileVC.instance()
        vc.user = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func follow(_ user: User, completion: ((Bool, Bool)->())?) {
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        if !AppDelegate.checkUser(self) { return }
        APIClient().request(request: .FollowUser(vc: self, userID: user.id)) { (response, success) in
            completion?(success, response.data["data"] as? Bool ?? false)
        }
    }
    
    func showServerBadErrorResponse() {
        AlertsController.showAlert(delegate: self, message: NSLocalizedString("Something went wrong with the server response", comment: ""), OKButtonTitle: "Aceptar")
    }
}
