//
//  UIColor+Ext.swift
//  Homing
//
//  Created by Miguel Jimenez on 22/01/18.
//  Copyright © 2018 oOMovil. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var main: UIColor {
        return #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
    }
}
