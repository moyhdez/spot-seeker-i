//
//  UIStoryboard+Ext.swift
//  spotseeker
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func initialViewController<T: UIViewController>() -> T {
        let vc = self.instantiateInitialViewController() as! T
        vc.removeBackTitle()
        return vc
    }
}
