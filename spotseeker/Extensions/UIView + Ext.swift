//
//  UIView+CustomView.swift
//  spotseeker
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension UIView {
    
    func roundCornerAndShadow() {
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5

    }
    
    func setCornerRadius(_ amount: CGFloat) {
        self.layer.cornerRadius = amount
        self.layer.masksToBounds = true
    }
    
    func setCircular() {
        self.layer.cornerRadius = self.frame.size.width / 2
        self.layer.masksToBounds = false
        self.clipsToBounds = true
    }
    
    func lightBorder() {
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.2
    }
    
    func addShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.15
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3
    }
    
    func setGradientBackground(topColor: UIColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), bottomColor: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ topColor.cgColor, bottomColor.cgColor]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.bounds
    
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    public func findViewsOfClass<T:UIView>(viewClass: T.Type) -> [T] {
        var views: [T] = []
        for subview in subviews {
            if subview is T {
                views.append(subview as! T)
            }
            views.append(contentsOf: subview.findViewsOfClass(viewClass: T.self))
        }
        return views
    }
    
    func fadeTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        self.layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
    
    func visibilityWithAlpha(isHidden: Bool) {
        self.isHidden = isHidden
        self.alpha = isHidden ? 0 : 1
    }
    
    func setAspectRatio(multiplier: CGFloat) {
        let constraint = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: self, attribute: .width, multiplier: multiplier,constant: 0)
        self.addConstraint(constraint)
    }
    
    public func fillSuperview(marginHorizontal: CGFloat = 0, marginVertical: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        if let superview = superview {
            leftAnchor.constraint(equalTo: superview.leftAnchor, constant: marginHorizontal).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor, constant: marginHorizontal).isActive = true
            topAnchor.constraint(equalTo: superview.topAnchor, constant: marginVertical).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: marginVertical).isActive = true
        }
    }
}
