//
//  UIButton + Ext.swift
//  spotseeker
//
//  Created by Moy Hdez on 15/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

extension UIButton {
    
    func style() {
        self.titleLabel?.numberOfLines = 1
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.titleLabel?.lineBreakMode = . byClipping
        self.setCornerRadius(1)
        self.addShadow()
    }
}
