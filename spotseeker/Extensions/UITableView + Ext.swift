//
//  UITableView+Ext.swift
//  spotseeker
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension UITableView {
    
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
    
    func resizeableCell(estimatedHeight: CGFloat) {
        self.estimatedRowHeight = estimatedHeight
        self.rowHeight = UITableView.automaticDimension
    }
    
    func register<T: UITableViewCell>(_: T.Type) {
        
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath as IndexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        
        return cell
    }
    
    func dequeueReusableCell<T: UITableViewCell>() -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        
        return cell
    }
    
    /// Set table header view & add Auto layout.
    func setTableHeaderView(headerView: UIView) {
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        // Set first.
        self.tableHeaderView = headerView
        
        // Then setup AutoLayout.
        headerView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    }
    
    /// Update header view's frame.
    func updateHeaderViewFrame() {
        guard let headerView = self.tableHeaderView else { return }
        
        // Update the size of the header based on its internal content.
        headerView.layoutIfNeeded()
        
        // ***Trigger table view to know that header should be updated.
        let header = self.tableHeaderView
        header?.autoresizingMask = []
        self.tableHeaderView = header
    }
    
    public func relayoutTableHeaderView() {
        if let tableHeaderView = self.tableHeaderView {
            let labels = tableHeaderView.findViewsOfClass(viewClass: UILabel.self)
            for label in labels {
                label.preferredMaxLayoutWidth = label.frame.width
            }
            tableHeaderView.setNeedsLayout()
            tableHeaderView.layoutIfNeeded()
            var frame = tableHeaderView.frame
            frame.size.height = tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            tableHeaderView.frame = frame
            self.tableHeaderView = tableHeaderView
        }
    }
    
    func scrollToBottom() {
        if self.numberOfRows(inSection: 0) == 0 {
            return
        }
        let lastIP = IndexPath.init(row: self.numberOfRows(inSection: 0) - 1, section: 0)
        self.scrollToRow(at: lastIP, at: .bottom, animated: true)
    }
}

extension UITableViewCell: ReusableView, NibLoadableView {}

extension UIRefreshControl {
    func refreshManually() {
        UIView.animate(withDuration: 0.3) {
            if let scrollView = self.superview as? UIScrollView {
                scrollView.setContentOffset(CGPoint(x: 0, y: -self.frame.height), animated: false)
            }
            self.beginRefreshing()
        }
    }
}
