//
//  UITabBarController + Ext.swift
//  spotseeker
//
//  Created by Moy Hdez on 15/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

extension UITabBarController {
    open override var childForStatusBarStyle: UIViewController? {
        return selectedViewController
    }
}
