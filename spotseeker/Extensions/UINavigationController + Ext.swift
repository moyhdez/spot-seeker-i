//
//  UITextField+-Extension.swift
//  spotseeker
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    open override var childForStatusBarStyle: UIViewController? {
        return visibleViewController
    }
    
    func pushViewController(viewController: UIViewController, animated: Bool, completion: (() -> ())?) {
        guard let completion = completion else { self.popViewController(animated: true); return }
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: true)
        CATransaction.commit()
    }
    
    func popViewController(animated: Bool, completion: (() -> ())?) {
        guard let completion = completion else { self.popViewController(animated: true); return }
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popViewController(animated: true)
        CATransaction.commit()
    }
}
