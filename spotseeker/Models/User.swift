//
//  User.swift
//  spotseeker
//
//  Created by Moy Hdez on 24/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class User: Codable {
    
    var id: Int
    var name: String
    var summary: String?
    var email: String
    var image: URL
    var spotsCount: Int?
    var followersCount: Int?
    var followingCount: Int?
    var premium: Int?
    var following: Bool = false
    var registrationMode: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case email
        case image
        case summary = "description"
        case spotsCount = "spots_quantity"
        case followersCount = "followers_quantity"
        case followingCount = "following_quantity"
        case premium
        case following = "is_following"
        case registrationMode = "registration_mode"
    }
    
    func isFromFacebook() -> Bool {
        return registrationMode == "facebook"
    }
    
    // MARK: - METHODS
    func saveToDefaults() {
        let encoder = JSONEncoder()
        let data = try! encoder.encode(self)
        UserDefaults.standard.set(data, forKey: K.Defaults.User)
    }
    
    public static func getFromDefaults() -> User? {
        guard let data = UserDefaults.standard.object(forKey: K.Defaults.User) as? Data else { return nil }
        let decoder = JSONDecoder()
        let user = try! decoder.decode(User.self, from: data)
        return user
    }
    
    static func destroy() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: K.Defaults.User)
        defaults.removeObject(forKey: K.Defaults.AccessToken)
        defaults.removeObject(forKey: K.Defaults.RefreshToken)
    }
    
    func clone(with user: User) {
        self.name = user.name
        self.summary = user.summary
        self.email = user.email
        self.image = user.image
        self.spotsCount = user.spotsCount
        self.followersCount = user.followersCount
        self.followingCount = user.followingCount
        self.premium = user.premium
        self.following = user.following
        self.registrationMode = user.registrationMode
    }
}
