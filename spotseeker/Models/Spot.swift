//
//  Spot.swift
//  spotseeker
//
//  Created by Moy Hdez on 11/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

class Spot: Codable {
    
    var id: Int?
    var title: String?
    var summary: String?
    var lat: Double?
    var lng: Double?
    var difficulty: Int?
    var images: [String] = []
    var videos: [String] = []
    var user: User?
    var category: Category?
    var favorite: Bool = false
    var comments: [Comment] = []
    var favoriteCount: Int?
    var commentsCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case summary
        case lat
        case lng
        case images
        case videos
        case difficulty
        case user
        case category
        case favorite
        case comments
        case favoriteCount = "favorite_count"
        case commentsCount = "comments_count"
    }
    
    func coordinates() -> CLLocationCoordinate2D? {
        guard let lat = self.lat, let lng = self.lng else { return nil }
        let coordinates = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        return coordinates
    }
    
    func distance(to location: CLLocationCoordinate2D) -> Double {
        guard let lat = self.lat, let lng = self.lng else { return 10000 }
        let from = CLLocation(latitude: lat, longitude: lng)
        let to = CLLocation(latitude: location.latitude, longitude: location.longitude)
        return from.distance(from: to)
    }
    
    func clone(with spot: Spot) {
        //FOR UPDATE BETWEEN SCREENS
        self.id = spot.id
        self.title = spot.title
        self.summary = spot.summary
        self.lat = spot.lat
        self.lng = spot.lng
        self.difficulty = spot.difficulty
        self.images = spot.images
        self.videos = spot.videos
        self.user = spot.user
        self.category = spot.category
        self.favorite = spot.favorite
        self.comments = spot.comments
        self.favoriteCount = spot.favoriteCount
        self.commentsCount = spot.commentsCount
    }
}
