//
//  Category.swift
//  spotseeker
//
//  Created by Moy Hdez on 17/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

class Category: Codable {
    var id: Int
    var image: URL?
    var title: String
//    var summary: String
    
    init(id: Int, image: URL? = nil, title: String) {
        self.id = id
        self.image = image
        self.title = title
    }
}

