//
//  NotificationLocal.swift
//  spotseeker
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

struct NotificationLocal: Codable {
    var id: String
    var type: String
    var data: NotificationData
    var readAt: String?
    var createdAt: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case type
        case data
        case readAt = "read_at"
        case createdAt = "created_at"
    }
}
