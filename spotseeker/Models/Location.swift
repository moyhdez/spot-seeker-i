//
//  Location.swift
//  spotseeker
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

class Location {
    var title: String!
    var subtitle: String!
    var coordinates: CLLocationCoordinate2D!
    
    init(title: String, subtitle: String, coordinates: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinates = coordinates
    }
}
