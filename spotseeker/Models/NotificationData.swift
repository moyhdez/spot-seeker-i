//
//  NotificationData.swift
//  spotseeker
//
//  Created by Moy Hdez on 23/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

struct NotificationData: Codable {
    var spotId: Int?
    var userId: Int?
    var articleId: Int?
    var image: URL
    var title: String
    var content: String
    
    enum CodingKeys: String, CodingKey {
        case spotId = "spot_id"
        case userId = "user_id"
        case articleId = "article_id"
        case image
        case title
        case content
    }
}
