//
//  FeedPost.swift
//  spotseeker
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

struct FeedPost: Codable {
    var id: Int
    var image: URL
    var title: String
    var content: String
    var htmlContent: String
    var updatedAt: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case image
        case title
        case content
        case htmlContent = "html_content"
        case updatedAt = "updated_at"
    }
}
