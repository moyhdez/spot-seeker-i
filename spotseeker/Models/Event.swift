//
//  Event.swift
//  spotseeker
//
//  Created by Moy Hdez on 18/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import Foundation

struct Event: Codable {
    var id: Int
    var name: String?
    var summary: String?
    var image: URL?
    var link: URL?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case summary = "description"
        case image
        case link
    }
}
