//
//  FeedDetailVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 20/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import WebKit

class FeedDetailVC: UIViewController, RefreshableViewController {
    var webView: WKWebView!
    var refreshControl: UIRefreshControl!
    
    var post: FeedPost!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        let jscript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        webView = WKWebView(frame: .zero, configuration: wkWebConfig)
        self.view.addSubview(webView)
        webView.fillSuperview()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            webView.scrollView.refreshControl = refreshControl
        } else {
            webView.scrollView.addSubview(refreshControl)
        }
        
        self.fillData()
    }
    
    func fillData() {
        webView.loadHTMLString(post.htmlContent, baseURL: nil)
    }
    
    @objc func refresh(sender: AnyObject) {
        getFeedPost()
    }

}

extension FeedDetailVC {
    
    func getFeedPost() {
        let id = post.id
        APIClient().request(request: .GetFeedPost(vc: self, id: id)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success, let post = try? FeedPost(response.data) {
                self.post = post
                self.fillData()
            }
        }
    }
}
