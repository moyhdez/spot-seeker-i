//
//  MainTabBarController.swift
//  spotseeker
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var userInfo: [AnyHashable: Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = .main
        self.tabBar.barTintColor = .white
        self.tabBar.isTranslucent = true
        
        let home = HomeVC.instanceAsNc()
        let homeItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "inicio_incactive_tab"), selectedImage: #imageLiteral(resourceName: "inicio_active_tab"))
        home.tabBarItem = homeItem
        
        let search = SearchVC.instanceAsNc()
        let searchItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "search_inactive_tab"), selectedImage: #imageLiteral(resourceName: "search_active_tab"))
        search.tabBarItem = searchItem
        
        let createSpot = CreateSpotVC.instanceAsNc()
        let createSpotItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "add_spot_inactive_tab"), selectedImage: #imageLiteral(resourceName: "add_spot_active_tab"))
        createSpot.tabBarItem = createSpotItem
        
        let favorites = FavoritesVC.instanceAsNc()
        let favoritesItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "fav_inactive_tab"), selectedImage: #imageLiteral(resourceName: "fav_active_tab"))
        favorites.tabBarItem = favoritesItem
        
        let profile = ProfileVC.instanceAsNc()
        let profileItem = UITabBarItem(title: "", image: #imageLiteral(resourceName: "user_inactive_tab"), selectedImage: #imageLiteral(resourceName: "user_active_tab"))
        profile.tabBarItem = profileItem
        
        
        let controllers = [home, search, createSpot, favorites, profile]
        self.viewControllers = controllers
        
        if let items = tabBar.items {
            for item in items {
                item.imageInsets = UIEdgeInsets(top: 7, left: 0, bottom: -7, right: 0)
            }
        }
        self.delegate = self
        
        if let _ = User.getFromDefaults() {
            AppDelegate.sharedInstance().registerForPushNotifications()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let userInfo = userInfo {
            AppDelegate.sharedInstance().handle(userInfo)
            self.userInfo = nil
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if let nc = viewController as? UINavigationController, let vc = nc.viewControllers.first, vc is HomeVC {
            return true
        } else if let nc = viewController as? UINavigationController, let vc = nc.viewControllers.first, vc is SearchVC {
            return true
        } else if User.getFromDefaults() == nil {
            let vc = LoginVC.instanceAsNc()
            self.present(vc, animated: true, completion: nil)
            return false
        } else if let nc = viewController as? UINavigationController, let vc = nc.viewControllers.first, vc is CreateSpotVC {
            let vc = CreateSpotVC.instanceAsNc()
            self.present(vc, animated: true, completion: nil)
            return false
        } else {
            return true
        }
    }
}
