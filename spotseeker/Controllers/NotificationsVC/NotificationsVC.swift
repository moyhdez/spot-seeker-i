//
//  NotificationsVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class NotificationsVC: UITableViewController {
    
    var pagination: Pagination?
    var isLoading = false
    
    var notifications = [NotificationLocal]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("Notifications", comment: "")
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(NotificationCell.self)
        tableView.register(EmptyStateCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl!)
        }
        getNotifications()
    }
    
    @objc func refresh(sender: AnyObject) {
        clearAndGetNotifications()
    }
    
    func clearAndGetNotifications() {
        pagination = nil
        getNotifications()
    }
    
}

// MARK: - TableView
extension NotificationsVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if notifications.isEmpty { return 1 }
        return notifications.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if notifications.isEmpty { return tableView.dequeueReusableCell(for: indexPath) as EmptyStateCell }
        let cell =  tableView.dequeueReusableCell(for: indexPath) as NotificationCell
        cell.set(for: notifications[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let pagination = pagination else { return }
        if indexPath.row == notifications.count - 1 && pagination.currentPage < pagination.lastPage && !isLoading{
            getNotifications()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if notifications.isEmpty { return }
        let data = notifications[indexPath.row].data
        if let spotId = data.spotId {
            self.getSpot(id: spotId)
        } else if let userId = data.userId {
            self.getUser(id: userId)
        } else if let postId = data.articleId {
            self.getFeedPost(id: postId)
        }
    }
}

// MARK: - Requests
extension NotificationsVC {
    
    func getNotifications() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        isLoading = true
        APIClient().request(request: .Notifications(vc: self, page: page)) { [weak self] (response, success) in
            self?.isLoading = false
            guard let `self` = self else { return }
            if success {
                guard let notificationsData = response.data["notifications"] as? [JSON],
                    let paginationData = response.data["pagination"] as? JSON,
                    let notifications = try? notificationsData.compactMap(NotificationLocal.init),
                    let  pagination = try? Pagination(paginationData) else { self.showServerBadErrorResponse(); return }
                
                
                self.pagination = pagination

                if self.pagination?.currentPage ?? 1 == 1 {
                    self.notifications = []
                }

                self.notifications.append(contentsOf: notifications)
            }
        }
    }
    
    func getUser(id: Int) {
        APIClient().request(request: .GetUser(vc: self, id: id)) { [weak self] (response, success) in
            if success, let `self` = self {
                guard let userDic = response.data["user"] as? JSON
                    , let user = try? User(userDic) else { self.showServerBadErrorResponse(); return }
                let vc = ProfileVC.instance()
                vc.user = user
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getSpot(id: Int) {
        APIClient().request(request: .Spot(vc: self, id: id)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success {
                guard let spotJSON = response.data["spot"] as? JSON, let spot = try? Spot(spotJSON) else { self.showServerBadErrorResponse(); return }
                let vc = DetailSpotVC.instance()
                vc.spot = spot
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getFeedPost(id: Int) {
        APIClient().request(request: .GetFeedPost(vc: self, id: id)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success {
                guard let post = try? FeedPost(response.data) else { self.showServerBadErrorResponse(); return  }
                let vc = FeedDetailVC.instance()
                vc.post = post
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
