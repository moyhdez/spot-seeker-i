//
//  SearchVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class SearchVC: UITableViewController {
    
    var searchDelayer: Timer = Timer()
    
    var items: [[Any]] {
        get {
            var arr: [[Any]] = []
            if !users.isEmpty {
                arr.append(users)
            }
            if !spots.isEmpty {
                arr.append(spots)
            }
            if !events.isEmpty {
                arr.append(events)
            }
            if !promos.isEmpty {
                arr.append(promos)
            }
            return arr
        }
    }
    
    var spots: [Spot] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var users: [User] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var events: [Event] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var promos: [Promotion] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
        searchController.searchBar.barStyle = .default
        searchController.searchBar.delegate = self
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
        } else {
            navigationItem.titleView = searchController.searchBar
            self.searchController.hidesNavigationBarDuringPresentation = false
            self.searchController.searchBar.searchBarStyle = .minimal
        }
        
        self.navigationItem.title = NSLocalizedString("Search", comment: "")
    
        tableView.register(SpotCell.self)
        tableView.register(UserSearchCell.self)
        tableView.register(EventSearchCell.self)
        tableView.register(PromoSearchCell.self)
        tableView.register(EmptyStateCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl!)
        }
    }
    
    @objc func refresh(sender: AnyObject) {
        search(searchController.searchBar.text)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.searchController.searchBar.becomeFirstResponder()
    }
    
}

extension SearchVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        search(searchBar.text)
        searchController.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchDelayer.invalidate()
        searchDelayer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchFromDelayer), userInfo: nil, repeats: false)
    }
    
    @objc func searchFromDelayer() {
        search(searchController.searchBar.text)
    }
}

// MARK: - TableView
extension SearchVC {
    
    @objc func didTapSlideShow(_ sender: UITapGestureRecognizer){
        guard let row = sender.view?.tag else { return }
        let index = IndexPath(row: row, section: 0)
        spotCellTapped(at: index)
    }
    
    @objc func topBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        if !AppDelegate.checkUser(self) { return }
        let indexPath = IndexPath(row: row, section: 0)
        spots[row].favorite = !spots[row].favorite
        tableView.reloadRows(at: [indexPath], with: .none)
        addToFavorites(indexPath, btn: sender)
    }
    
    @objc func distanceBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        let spot = spots[row]
        if let coordinates = spot.coordinates() {
            AppDelegate.openMap(self, with: coordinates)
        }
    }
    
    @objc func goToUser(_ sender: UIButton) {
        let row = sender.tag
        guard let user = spots[row].user else { return }
        self.goToProfile(of: user)
    }
    
    @objc func followUser(_ sender: UIButton) {
        let row = sender.tag
        self.follow(users[row]) { success, following in
            if success {
                self.users[row].following = following
                self.tableView.reloadData()
            }
        }
    }
    
    func setUp(_ cell: SpotCell, for indexPath: IndexPath) {
        let row = indexPath.row
        cell.setUpWith(spot: spots[row])
        
        cell.distanceBtn.tag = row
        cell.topBtn.tag = row
        cell.userBtn.tag = row
        cell.distanceBtn.addTarget(self, action: #selector(distanceBtnPressed(_:)), for: .touchUpInside)
        cell.topBtn.addTarget(self, action: #selector(topBtnPressed(_:)), for: .touchUpInside)
        cell.userBtn.addTarget(self, action: #selector(goToUser(_:)), for: .touchUpInside)
        let slideShowTGR = UITapGestureRecognizer(target: self, action: #selector(self.didTapSlideShow(_:)))
        cell.slideshow.tag = row
        cell.slideshow.addGestureRecognizer(slideShowTGR)
    }
    
    func spotCellTapped(at index: IndexPath) {
        let vc = DetailSpotVC.instance()
        vc.spot = spots[index.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func userCellTapped(at index: IndexPath) {
        self.goToProfile(of: users[index.row])
    }
    
    func eventCellTapped(at index: IndexPath) {
        if let url = events[index.row].link {
            AppDelegate.open(url)
        }
    }
    
    func promoCellTapped(at index: IndexPath) {
        if let url = promos[index.row].link {
            AppDelegate.open(url)
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if items.isEmpty { return 1 }
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.isEmpty { return 1 }
        return items[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if items.isEmpty { return tableView.dequeueReusableCell(for: indexPath) as EmptyStateCell }
        switch type(of: items[indexPath.section][indexPath.row]) {
        case is User.Type:
            let cell =  tableView.dequeueReusableCell(for: indexPath) as UserSearchCell
            cell.addUserBtn.tag = indexPath.row
            cell.addUserBtn.addTarget(self, action: #selector(followUser(_:)), for: .touchUpInside)
            if let user = items[indexPath.section][indexPath.row] as? User {
                cell.set(for: user)
            }
            return cell
        case is Event.Type:
            let cell =  tableView.dequeueReusableCell(for: indexPath) as EventSearchCell
            if let event = items[indexPath.section][indexPath.row] as? Event {
                cell.set(for: event)
            }
            return cell
        case is Promotion.Type:
            let cell =  tableView.dequeueReusableCell(for: indexPath) as PromoSearchCell
            if let promo = items[indexPath.section][indexPath.row] as? Promotion {
                cell.set(for: promo)
            }
            return cell
        case is Spot.Type:
            let cell =  tableView.dequeueReusableCell(for: indexPath) as SpotCell
            setUp(cell, for: indexPath)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if items.isEmpty { return }
        switch type(of: items[indexPath.section][indexPath.row]) {
        case is Spot.Type:
            spotCellTapped(at: indexPath)
        case is User.Type:
            userCellTapped(at: indexPath)
        case is Event.Type:
            eventCellTapped(at: indexPath)
        case is Promotion.Type:
            promoCellTapped(at: indexPath)
        default:
            return
        }
        
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if items.isEmpty { return nil }
        guard let item = items[section].first else { return nil }
        switch type(of: item) {
        case is User.Type:
            return NSLocalizedString("Users", comment: "")
        case is Event.Type:
            return NSLocalizedString("Events", comment: "")
        case is Promotion.Type:
            return NSLocalizedString("Promotions", comment: "")
        case is Spot.Type:
            return NSLocalizedString("Spots", comment: "")
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if items.isEmpty { return 200 }
        switch type(of: items[indexPath.section][indexPath.row]) {
        case is User.Type:
            return 60
        case is Event.Type:
            return 300
        case is Promotion.Type:
            return 120
        case is Spot.Type:
            return 500
        default:
            return 500
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if items.isEmpty { return UITableView.automaticDimension }
        switch type(of: items[indexPath.section][indexPath.row]) {
        case is User.Type:
            return 60
        case is Promotion.Type:
            return 120
        default:
            return UITableView.automaticDimension
        }
    }
}

// MARK: - Requests
extension SearchVC {
    
    func addToFavorites(_ indexPath: IndexPath, btn: UIButton) {
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spots[indexPath.row].id else { return }
        
        APIClient().request(request: .AddToFavorites(vc: self, id: spotId)) { [weak self] (response, success) in
            if success, let `self` = self, let favorite = response.data["favorite"] as? Bool, !favorite {
                self.spots[indexPath.row].favorite = favorite
            }
        }
    }
    
    func search(_ text: String?) {
        guard let text = text else { return }
        APIClient().request(request: .Search(vc: self, search: text)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success {
                if let spotsData = response.data["spots"] as? [JSON], let spots = try? spotsData.compactMap(Spot.init) {
                    self.spots = spots
                }
                if let usersData = response.data["users"] as? [JSON], let users = try? usersData.compactMap(User.init) {
                    self.users = users
                }
                if let eventsData = response.data["events"] as? [JSON], let events = try? eventsData.compactMap(Event.init) {
                    self.events = events
                }
                if let promosData = response.data["promotions"] as? [JSON], let promos = try? promosData.compactMap(Promotion.init) {
                    self.promos = promos
                }
            }
        }
    }
}
