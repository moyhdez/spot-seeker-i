//
//  LoginVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 24/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//
//

import UIKit
import Simplicity
import TextFieldEffects
import Firebase

class LoginVC: UIViewController {
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var exitBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var forgotPassBtn: UIButton!
    @IBOutlet weak var userTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var logoImgView: UIImageView!
    @IBOutlet weak var txtsContainer: UIView!

    var isInLoginByEmail = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if User.getFromDefaults() != nil {
            let vc = MainTabBarController()
            AppDelegate.sharedInstance().switchRootViewController(vc: vc, animated: false)
        }
        styleViews()
        setUpViewsVisibility()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let _ = User.getFromDefaults() {
            User.destroy()
            AlertsController.showAlert(delegate: self, message: NSLocalizedString("Your session has expired", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
    
    func validator() {
        let email = userTxt.text!
        let password = passTxt.text!
        
        if !email.isEmpty &&  !password.isEmpty {
            login()
        } else {
            AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: NSLocalizedString("All fields are required", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""))
        }
    }
    
    func goToHome() {
        let vc = MainTabBarController()
        AppDelegate.sharedInstance().switchRootViewController(vc: vc, animated: true)
    }
    
    func setUpViewsVisibility() {
        self.facebookBtn.visibilityWithAlpha(isHidden: !self.isInLoginByEmail)
        self.registerBtn.visibilityWithAlpha(isHidden: !self.isInLoginByEmail)
        self.cancelBtn.visibilityWithAlpha(isHidden: self.isInLoginByEmail)
        
        self.titleLbl.text = NSLocalizedString(self.isInLoginByEmail ? "Log in" : "Recover password", comment: "")
        
        self.passTxt.visibilityWithAlpha(isHidden: !self.isInLoginByEmail)
        self.emailBtn.setTitle(!self.isInLoginByEmail ? NSLocalizedString("Send code", comment: "") : NSLocalizedString("Log in all caps", comment: ""), for: .normal)
        self.forgotPassBtn.setTitle(!self.isInLoginByEmail ? NSLocalizedString("Already hava a code", comment: "") : NSLocalizedString("Forgot my password", comment: ""), for: .normal)
    }
    
    func styleViews() {
        facebookBtn.style()
        emailBtn.style()
        registerBtn.style()
        logoImgView.setCornerRadius(1)
        
        let registerAttr = NSMutableAttributedString(string: NSLocalizedString("Don't have an account yet?", comment: ""), attributes: [.foregroundColor: UIColor.gray])
        registerAttr.append(.init(string: NSLocalizedString("Sing up complementary", comment: ""), attributes: [.foregroundColor: UIColor.black]))
        registerBtn.setAttributedTitle(registerAttr, for: .normal)
        facebookBtn.setTitle(NSLocalizedString("Login by Facebook", comment: ""), for: .normal)
        emailBtn.setTitle(NSLocalizedString("Log in all caps", comment: ""), for: .normal)
        titleLbl.text = NSLocalizedString("Log in", comment: "")
        cancelBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        exitBtn.setTitle(NSLocalizedString("Cancel", comment: ""), for: .normal)
        userTxt.placeholder = NSLocalizedString("Email", comment: "")
        passTxt.placeholder = NSLocalizedString("Password", comment: "")
    }
    
    func animateLoginByEmail() {
        self.isInLoginByEmail = !self.isInLoginByEmail
        self.view.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.15,animations:  {
            self.setUpViewsVisibility()
        }, completion: { (success) in
            self.view.isUserInteractionEnabled = true
        })
    }
    
    @IBAction func fbPressed(_ sender: UIButton) {
        Simplicity.login(Facebook()) { [weak self] (accessToken, error) in
            if let token = accessToken {
                self?.loginFacebook(token)
            }
        }
    }
    
    @IBAction func emailPressed(_ sender: UIButton) {
        if !isInLoginByEmail {
            recoverPass()
        } else {
            validator()
        }
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        let vc = RegisterVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func outOfLoginByEmail(_ sender: UIButton) {
        animateLoginByEmail()
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        if !self.isInLoginByEmail {
            let vc = UpdatePassVC.instance()
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            animateLoginByEmail()
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

}

// MARK: - TextfieldDelegate
extension LoginVC: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == userTxt {
            passTxt.becomeFirstResponder()
        } else if textField == passTxt {
            validator()
        }
        return true
    }
}

//MARK: REQUEST
extension LoginVC {
    
    func login() {
        let params: JSON = [
            "username": userTxt.text ?? "",
            "password": passTxt.text ?? ""
        ]
        
        APIClient().request(request: .LoginByEmail(vc: self, params: params)) { [weak self] (response ,success) in
            if success {
                self?.handle(response)
            }
        }
    }
    
    func loginFacebook(_ accessToken: String) {
        let params: JSON = [
            "access_token": accessToken
        ]
        
        APIClient().request(request: .LoginFacebook(vc: self, params: params)) { [weak self] (response ,success) in
            if success {
                self?.handle(response)
            }
        }
    }
    
    func handle(_ response: ApiResponse) {
        guard let userDic = response.data["user"] as? JSON
            , let user = try? User(userDic), let oauth = response.data["oauth"] as? JSON else { return }
        
        user.saveToDefaults()
        UserDefaults.standard.setValue(oauth["access_token"] as? String, forKey: K.Defaults.AccessToken)
        UserDefaults.standard.setValue(oauth["refresh_token"] as? String, forKey: K.Defaults.RefreshToken)
        if let expiresIn = oauth["expires_in"] as? Double {
            UserDefaults.standard.setValue(Date().addingTimeInterval(expiresIn).toStringWith(format: K.Defaults.DateFormat), forKey: K.Defaults.TokenExpiresAt)
        }
        self.goToHome()
    }
    
    func recoverPass() {
        guard let email = userTxt.text, !email.isEmpty else {
            AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: NSLocalizedString("Please enter an email", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""))
            return
        }
        
        let params: JSON = [
            "email": email
        ]
        
        APIClient().request(request: .RecoverPassword(vc: self, params: params)) { [weak self] (response,success) in
            guard let `self` = self else { return }
            if success {
                let vc = UpdatePassVC.instance()
                vc.email = email
                vc.didUpdatePass = {
                    self.animateLoginByEmail()
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
}
