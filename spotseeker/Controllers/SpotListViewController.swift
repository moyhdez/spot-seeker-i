//
//  SpotListViewController.swift
//  spotseeker
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

class SpotListViewController: UIViewController, SpotList, RefreshableViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var locationManager = CLLocationManager()
    var userLocation: CLLocationCoordinate2D?
    var locationSent: CLLocationCoordinate2D?
    var isFirstTime: Bool = true
    
    var pagination: Pagination?
    var isLoading = false
    
    var onRefresh: (()->())?
    
    var firstTimeLocationUpdate: (()->())?
    
    var spots = [Spot]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(SpotCell.self)
        tableView.register(EmptyStateCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        setUpLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    @objc func refresh(sender: AnyObject) {
        clearAndGetSpots()
        onRefresh?()
    }
    
    func setUpLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .restricted, .denied:
                getSpots()
                break
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.startUpdatingLocation()
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            @unknown default:
                break
            }
        } else {
            getSpots()
        }
    }
    
    func clearAndGetSpots() {
        pagination = nil
        locationSent = nil
        getSpots()
    }
    
    func getSpots() {
        
    }
    
}

// MARK: - TableView
extension SpotListViewController: UITableViewDelegate, UITableViewDataSource {
    
    @objc func didTapSlideShow(_ sender: UITapGestureRecognizer){
        guard let row = sender.view?.tag else { return }
        let index = IndexPath(row: row, section: 0)
        cellTapped(at: index)
    }
    
    @objc func topBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        if !AppDelegate.checkUser(self) { return }
        let indexPath = IndexPath(row: row, section: 0)
        spots[row].favorite = !spots[row].favorite
        tableView.reloadRows(at: [indexPath], with: .none)
        addToFavorites(indexPath, btn: sender)
    }
    
    @objc func distanceBtnPressed(_ sender: UIButton) {
        let row = sender.tag
        let spot = spots[row]
        if let coordinates = spot.coordinates() {
            AppDelegate.openMap(self, with: coordinates)
        }
    }
    
    @objc func goToUser(_ sender: UIButton) {
        let row = sender.tag
        guard let user = spots[row].user else { return }
        self.goToProfile(of: user)
    }
    
    func setUp(_ cell: SpotCell, for indexPath: IndexPath) {
        let row = indexPath.row
        cell.setUpWith(spot: spots[row], location: userLocation)
        
        cell.distanceBtn.tag = row
        cell.topBtn.tag = row
        cell.userBtn.tag = row
        cell.distanceBtn.addTarget(self, action: #selector(distanceBtnPressed(_:)), for: .touchUpInside)
        cell.topBtn.addTarget(self, action: #selector(topBtnPressed(_:)), for: .touchUpInside)
        cell.userBtn.addTarget(self, action: #selector(goToUser(_:)), for: .touchUpInside)
        let slideShowTGR = UITapGestureRecognizer(target: self, action: #selector(self.didTapSlideShow(_:)))
        cell.slideshow.tag = row
        cell.slideshow.addGestureRecognizer(slideShowTGR)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if spots.isEmpty { return 1 }
        return spots.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if spots.isEmpty { return tableView.dequeueReusableCell(for: indexPath) as EmptyStateCell }
        let cell =  tableView.dequeueReusableCell(for: indexPath) as SpotCell
        setUp(cell, for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? SpotCell {
            let spot = spots[indexPath.row]
            if let location = userLocation {
                cell.distanceLbl.text = spot.coordinates()?.readableDistance(to: location)
            } else {
                cell.distanceLbl.text = NSLocalizedString("Go", comment: "")
            }
        }
        guard let pagination = pagination else { return }
        if indexPath.row == spots.count - 1 && pagination.currentPage < pagination.lastPage && !isLoading {
            getSpots()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if spots.isEmpty { return }
        cellTapped(at: indexPath)
    }
    
    func cellTapped(at index: IndexPath) {
        let vc = DetailSpotVC.instance()
        vc.spot = spots[index.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK: - Location manager
extension SpotListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if let coordinates = userLocation {
                if coordinates.latitude != location.coordinate.latitude && coordinates.longitude != location.coordinate.longitude{
                    userLocation = location.coordinate
                }
            } else {
                userLocation = location.coordinate
                clearAndGetSpots()
            }
            if isFirstTime {
                firstTimeLocationUpdate?()
                isFirstTime = false
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
        case .restricted, .denied:
            userLocation = nil
            clearAndGetSpots()
            UserDefaults.standard.set(false, forKey: "didShowLocationAlert")
            break
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        default: break
        }
    }
}

// MARK: - Requests
extension SpotListViewController {
    
    func addToFavorites(_ indexPath: IndexPath, btn: UIButton) {
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spots[indexPath.row].id else { return }
        
        APIClient().request(request: .AddToFavorites(vc: self, id: spotId)) { [weak self] (response, success) in
            if success, let `self` = self, let favorite = response.data["favorite"] as? Bool, !favorite {
                self.spots[indexPath.row].favorite = favorite
            }
        }
    }
    
    func getSpots(request: ApiRequest) {
        isLoading = true
        APIClient().request(request: request) { [weak self] (response, success) in
            self?.isLoading = false
            guard let `self` = self else { return }
            if success {
                guard let spotsData = response.data["spots"] as? [JSON],
                    let paginationData = response.data["pagination"] as? JSON,
                    let  pagination = try? Pagination(paginationData),
                    var spots = try? spotsData.compactMap(Spot.init) else { self.showServerBadErrorResponse(); return }
                
                self.pagination = pagination
                
                if self.pagination?.currentPage ?? 1 == 1 {
                    self.spots = []
                }
                
                if let location = self.userLocation {
                    spots = spots.sorted(by: {$0.distance(to: location) < $1.distance(to: location)})
                }
                
                self.spots.append(contentsOf: spots)
            }
        }
    }
}
