//
//  FilterVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 22/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class FilterVC: UIViewController, RefreshableViewController {
    
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var difficultyStack: UIStackView!
    var refreshControl: UIRefreshControl!
    
    var homeVC: HomeVC!

    var categories: [Category] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var selectedMaxDifficulty: Int = 5 {
        didSet {
            updateDiffBtns()
        }
    }
    
    var selectedCategories: [Int] = [] {
        didSet {
            if selectedCategories.count == categories.count - 1 && !selectedCategories.contains(0) {
                selectedCategories.append(0)
            }
            tableView.reloadData()
        }
    }
    
    
    var allCategories = Category.init(id: 0, title: "Todas las categorías")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Filter", comment: "")
        
        if let maxDistanceStr = homeVC.maxDistance, let maxDistance = Float(maxDistanceStr) {
            distanceSlider.value = maxDistance
            distanceLbl.text = "\(Int(distanceSlider.value)) km"
        }
        
        if let difficulties = homeVC.params["difficulties[]"] as? [Int], let maxDiff = difficulties.sorted().last {
            self.selectedMaxDifficulty = maxDiff
        } else {
            updateDiffBtns()
        }
        
        if let selectedCategories = homeVC.params["categories[]"] as? [Int] {
            self.selectedCategories = selectedCategories
        }
        
        for (i, btn) in difficultyStack.arrangedSubviews.compactMap({ $0 as? UIButton }).enumerated() {
            btn.tag = i + 1
            btn.addTarget(self, action: #selector(selectDiff), for: .touchUpInside)
        }
        
        setUpTableView()
        categories.append(allCategories)
        getCategories()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.relayoutTableHeaderView()
    }
    
    func setUpTableView() {
        tableView.rowHeight = 45
        tableView.register(CategoryFilterCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func updateDiffBtns() {
        for btn in difficultyStack.arrangedSubviews.compactMap({ $0 as? UIButton }) {
            btn.setImage(#imageLiteral(resourceName: "wheel"), for: .normal)
        }
        for dif in 0..<selectedMaxDifficulty {
            if let btn = difficultyStack.arrangedSubviews[dif] as? UIButton {
                btn.setImage(#imageLiteral(resourceName: "wheel active"), for: .normal)
            }
        }
    }
    
    @objc func refresh(sender: AnyObject) {
        getCategories()
    }
    
    @objc func selectDiff(sender: UIButton) {
        selectedMaxDifficulty = sender.tag
    }
    
    @IBAction func filter(_ sender: UIButton) {
        var categoriesAux = selectedCategories
        if let index = categoriesAux.firstIndex(where: { $0 == 0 }) {
            categoriesAux.remove(at: index)
        }
        let difficulties: [Int] = Array(1...selectedMaxDifficulty)
        let params: JSON = [
            "difficulties[]": difficulties,
            "categories[]": categoriesAux,
        ]
        
        homeVC.maxDistance = "\(Int(distanceSlider.value))"
        homeVC.params = params
        self.navigationController?.popViewController(animated: true) {
            self.homeVC.clearAndGetSpots()
            self.homeVC.clearAndGetMapSpots()
        }
    }

    @IBAction func distanceSliderChange(_ sender: Any) {
        distanceLbl.text = "\(Int(distanceSlider.value)) km"
    }
}

// MARK: - TableView
extension FilterVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(for: indexPath) as CategoryFilterCell
        let category = categories[indexPath.row]
        cell.set(for: category, selected: selectedCategories.contains(category.id))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = categories[indexPath.row]
        if category.id == 0 {
            if selectedCategories.count == categories.count {
                selectedCategories.removeAll()
            } else {
                selectedCategories = categories.map({ $0.id })
            }
        } else if let index = selectedCategories.firstIndex(where: { $0 == category.id }) {
            selectedCategories.remove(at: index)
            if selectedCategories.count == categories.count - 1, let index = selectedCategories.firstIndex(where: { $0 == 0 }) {
                selectedCategories.remove(at: index)
            }
        } else {
            selectedCategories.append(category.id)
        }
    }
}

extension FilterVC {
    
    func getCategories() {
        APIClient().request(request: .Categories(vc: self)) { [weak self] (response, success) in
            if success, let `self` = self {
                guard let categoriesJson = response.data["categories"] as? [JSON], var categories = try? categoriesJson.map(Category.init) else { self.showServerBadErrorResponse(); return }
                categories.insert(self.allCategories, at: 0)
                self.categories = categories
            }
        }
    }
}
