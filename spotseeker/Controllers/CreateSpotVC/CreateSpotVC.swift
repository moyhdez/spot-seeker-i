//
//  CreateSpotVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 20/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import AssetsPickerViewController
import Photos

class CreateSpotVC: UIViewController, RefreshableViewController {
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleTxt: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var addLocationBtn: UIButton!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var imagesTitleLbl: UILabel!
    @IBOutlet weak var desciptionTitleLbl: UILabel!
    @IBOutlet weak var difficultiTitleLbl: UILabel!
    @IBOutlet weak var difficultyDescLbl: UILabel!
    @IBOutlet weak var typeTitleLbl: UILabel!
    @IBOutlet weak var typeDescLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var difficultyStack: UIStackView!
    var refreshControl: UIRefreshControl!
    
    var location: CLLocationCoordinate2D?
    
    var didEdit: ((Spot)->())?
    
    var diff: Int = 0 {
        didSet {
            updateDiffBtns()
        }
    }
    
    var categories: [Category] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var selectedCategory: Category? {
        didSet {
            tableView.reloadData()
        }
    }
    
    var selectedAssets: [Any] = [] {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var deletedImgs: [String] = []
    
    var spot: Spot?

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .always
        }
        setTexts()
        fillData()
        descriptionTextView.delegate = self
        setUpStackDiff()
        updateDiffBtns()
        setUpTableView()
        getCategories()
        setUpCollectionView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.relayoutTableHeaderView()
    }
    
    func fillData() {
        if let spot = spot {
            selectedAssets = spot.images
            titleTxt.text = spot.title
            descriptionTextView.text = spot.summary
            addLocationBtn.setTitle(NSLocalizedString("Edit", comment: ""), for: .normal)
            diff = spot.difficulty ?? 0
            selectedCategory = spot.category
            self.location = spot.coordinates()
            updateSize(for: descriptionTextView)
        }
    }
    
    func setTexts() {
        self.title = spot == nil ? NSLocalizedString("Add new spot", comment: "") : NSLocalizedString("Edit spot", comment: "")
        navigationItem.setRightBarButton(UIBarButtonItem(title: NSLocalizedString("Ok", comment: ""), style: .plain, target: self, action: #selector(submit)), animated: true)
        navigationItem.setLeftBarButton(UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(close)), animated: true)
        imagesTitleLbl.text = NSLocalizedString("Images", comment: "")
        titleLbl.text = NSLocalizedString("Spot title", comment: "")
        desciptionTitleLbl.text = NSLocalizedString("Description", comment: "")
        locationLbl.text = NSLocalizedString("Location", comment: "")
        difficultiTitleLbl.text = NSLocalizedString("Difficulty", comment: "")
        difficultyDescLbl.text = NSLocalizedString("Select how difficult its the spot", comment: "")
        typeTitleLbl.text = NSLocalizedString("Type", comment: "")
        typeDescLbl.text = NSLocalizedString("Category description create spot", comment: "")
        addLocationBtn.setTitle(NSLocalizedString("Add", comment: ""), for: .normal)
    }
    
    func setUpStackDiff() {
        for (i, btn) in difficultyStack.arrangedSubviews.compactMap({ $0 as? UIButton }).enumerated() {
            btn.tag = i + 1
            btn.addTarget(self, action: #selector(selectDiff), for: .touchUpInside)
        }
    }
    
    func setUpTableView() {
        tableView.rowHeight = 45
        tableView.register(CategoryFilterCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    func setUpCollectionView() {
        collectionView.register(ImageCell.self)
        collectionView.delegate = self
        collectionView.dataSource = self
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .always
        }
    }
    
    func updateDiffBtns() {
        for btn in difficultyStack.arrangedSubviews.compactMap({ $0 as? UIButton }) {
            btn.setImage(diff >= btn.tag ? #imageLiteral(resourceName: "wheel active") : #imageLiteral(resourceName: "wheel"), for: .normal)
        }
    }
    
    func pickPhoto() {
        ImagePicker.takePhoto(vc: self)
    }
    
    @objc func refresh(sender: AnyObject) {
        getCategories()
    }
    
    @objc func selectDiff(sender: UIButton) {
        let diff = sender.tag
        self.diff = diff
    }
    
    @objc func submit() {
        self.view.endEditing(true)
        
        var message = ""
        
        if titleTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter a title", comment: "")
        } else if descriptionTextView.text.isEmpty {
            message = NSLocalizedString("Please enter a description", comment: "")
        } else if location == nil {
            message = NSLocalizedString("Please select a location", comment: "")
        } else if selectedAssets.isEmpty {
            message = NSLocalizedString("Please select some images", comment: "")
        } else if selectedCategory == nil {
            message = NSLocalizedString("Please select a category", comment: "")
        } else if diff == 0 {
            message = NSLocalizedString("Please select a difficulty", comment: "")
        } else {
            createParams()
            return
        }
        
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
    }
    
// MARK: - ACTIONS
    @objc func close() {
        if self.navigationController?.presentingViewController == nil {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        self.view.endEditing(true)
        let mapVC = SelectLocationVC.instance()
        mapVC.isSelectingLocation = true
        mapVC.delegate = self
        mapVC.location = location
        self.navigationController?.pushViewController(mapVC, animated: true)
    }
}

//MARK: - TEXTVIEW
extension CreateSpotVC: UITextViewDelegate {
    
    func updateSize(for textView: UITextView) {
        let sizeThatFits: CGSize = textView.sizeThatFits(CGSize(width: textView.frame.width, height: CGFloat(MAXFLOAT)))
        if sizeThatFits.height < 40 { return }
        descriptionHeight.constant = sizeThatFits.height
        tableView.relayoutTableHeaderView()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        updateSize(for: textView)
    }
}

// MARK: - TableView
extension CreateSpotVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(for: indexPath) as CategoryFilterCell
        let category = categories[indexPath.row]
        cell.set(for: category, selected: selectedCategory?.id ?? -1 == category.id)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let category = categories[indexPath.row]
        self.selectedCategory = category
    }
}

// MARK: - COLLECTIONVIEW
extension CreateSpotVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    @objc func deleteImage(_ sender: AnyObject) {
        let row: Int = sender.tag
        if let img = selectedAssets[row] as? String {
            deletedImgs.append(img)
        }
        selectedAssets.remove(at: row)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedAssets.count < 5 ? selectedAssets.count + 1 : selectedAssets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let imageCell = collectionView.dequeueReusableCell(for: indexPath) as ImageCell
        
        let row = selectedAssets.count < 5 ? indexPath.row - 1 : indexPath.row
        
        imageCell.deleteButton.tag = row
        imageCell.deleteButton.addTarget(self, action: #selector(self.deleteImage(_:)), for: .touchUpInside)
        imageCell.selectImageView.visibilityWithAlpha(isHidden: row >= 0)
        
        if row < 0 { imageCell.imageView.image = nil; return imageCell }
        
        let asset = selectedAssets[row]
        if let phasset = asset as? PHAsset {
            imageCell.imageView.image = phasset.getAssetThumbnail()
        } else if let image = asset as? UIImage {
            imageCell.imageView.image = image
        } else if let image = asset as? String, let url = URL(string: image) {
            imageCell.imageView.af_setImage(withURL: url)
        }
        
        return imageCell
    }
   
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 && selectedAssets.count < 5 {
            self.view.endEditing(true)
            handlePhoto()
        } else if selectedAssets.count >= 5 {
            let alert = UIAlertController(title: nil, message: NSLocalizedString("Max. 5 photos", comment: ""), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        }
    }
}

// MARK: - COLLECTIONVIEW LAYOUT
extension CreateSpotVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = collectionView.frame.height - 2
        return CGSize(width: side, height: side)
    }
}


// MARK: - REQUEST
extension CreateSpotVC {
    
    func getCategories() {
        APIClient().request(request: .Categories(vc: self)) { [weak self] (response, success) in
            if success, let `self` = self {
                guard let categoriesJson = response.data["categories"] as? [JSON], let categories = try? categoriesJson.map(Category.init) else { self.showServerBadErrorResponse(); return }
                self.categories = categories
            }
        }
    }
    
    func getBase64From(_ images: [UIImage]) -> String {
        return images.map({$0.base64()}).joined(separator: " ")
    }

    
    func createParams() {
        var images = selectedAssets.compactMap({ ($0 as? UIImage) })
        let title = titleTxt.text ?? ""
        let desc = descriptionTextView.text ?? ""
        let lat = location!.latitude
        let lng = location!.longitude
        
        let group = DispatchGroup()
        
        for phasset in selectedAssets.compactMap({ $0 as? PHAsset }) {
            group.enter()
            phasset.getUIImage { (image) in
                if let image = image {
                    images.append(image)
                }
                group.leave()
            }
        }
        
        group.notify(queue: .global(qos: .default)) {
            var params: JSON = [
                "title": title,
                "summary": desc,
                "lat": lat,
                "lng": lng,
                "category_id": self.selectedCategory?.id ?? 0,
                "difficulty": self.diff,
            ]
            
            if !images.isEmpty {
                params.updateValue(self.getBase64From(images), forKey: "images")
            }
            
            for (i, img) in self.deletedImgs.enumerated() {
                params.updateValue(img, forKey: "deleted_images[\(i)]")
            }
            
            DispatchQueue.main.async {
                self.sendData(params)
            }
        }
    }
    
    func sendData(_ params: JSON) {
        var request: ApiRequest!
        if let spot = spot, let id = spot.id {
            request = ApiRequest.UpdateSpot(vc: self, params: params, spotID: id)
        } else {
            request = ApiRequest.CreateSpot(vc: self, params: params)
        }
        
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        
        APIClient().request(request: request) { [weak self] (response, success) in
            print(response.data)
            if success, let `self` = self {
                guard let spotJson = response.data["spot"] as? [String:Any], let spot = try? Spot(spotJson) else { self.showServerBadErrorResponse(); return }
                self.didEdit?(spot)
                if self.spot == nil {
                    AlertsController.showAlert(delegate: self, message: NSLocalizedString("spot_created_success", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), success: {
                        self.close()
                    }, cancel: nil)
                } else {
                    self.close()
                }
            }
        }
    }
}

// MARK: - IMAGEPICKER LIBRARY
extension CreateSpotVC: UINavigationControllerDelegate, AssetsPickerViewControllerDelegate {
    
    @objc func handlePhoto() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Select photo", comment: ""), style: .default, handler: {
            action in
            let pickerConfig = AssetsPickerConfig()
            pickerConfig.albumIsShowEmptyAlbum = false
            pickerConfig.selectedAssets = self.selectedAssets.compactMap({return $0 as? PHAsset})
            let picker = AssetsPickerViewController()
            picker.pickerConfig = pickerConfig
            picker.pickerDelegate = self
            self.present(picker, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Take photo", comment: ""), style: .default, handler: {
            action in
            self.pickPhoto()
        }))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func assetsPickerCannotAccessPhotoLibrary(controller: AssetsPickerViewController) {
        
    }
    
    func assetsPickerDidCancel(controller: AssetsPickerViewController) {
        
    }
    
    func assetsPicker(controller: AssetsPickerViewController, shouldSelect asset: PHAsset, at indexPath: IndexPath) -> Bool {
        return (controller.selectedAssets.count + self.selectedAssets.filter({ !($0 is PHAsset) }).count) < 5
    }
    
    func assetsPicker(controller: AssetsPickerViewController, selected assets: [PHAsset]) {
        var aux = selectedAssets.filter({ !($0 is PHAsset) })
        
        for asset in assets {
            aux.append(asset)
        }
        self.selectedAssets = aux
    }
}


// MARK: - IMAGEPICKER CAMERA
extension CreateSpotVC: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedAssets.append(image.resizeImage(maxSize: 1000))
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedAssets.append(image.resizeImage(maxSize: 1000))
        } else { print ("error") }
        
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("image picker cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}
