//
//  EditProfileVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import TextFieldEffects

class EditProfileVC: UIViewController {
    @IBOutlet weak var editPassContainer: UIView!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var nameTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var aboutMeTxt: UITextView!
    @IBOutlet weak var aboutMeLbl: UILabel!
    @IBOutlet weak var aboutMeHeight: NSLayoutConstraint!
    @IBOutlet weak var confPassTxt: HoshiTextField!
    @IBOutlet weak var currPassTxt: HoshiTextField!
    @IBOutlet weak var changePassBtn: UIButton!
    @IBOutlet weak var saveBtn: UIButton!
    
    var user: User!
    
    var newImage: UIImage?
    
    var isChangingPass = false
    
    var didUpdate: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        guard let user = User.getFromDefaults() else {
            AppDelegate.logout()
            return
        }
        editPassContainer.isHidden = user.isFromFacebook()
        changePassBtn.isHidden = user.isFromFacebook()
        saveBtn.setTitle(NSLocalizedString("Save", comment: ""), for: .normal)
        nameTxt.placeholder = NSLocalizedString("Name", comment: "")
        aboutMeLbl.text = NSLocalizedString("About me", comment: "")
        currPassTxt.placeholder = NSLocalizedString("Current password", comment: "")
        passTxt.placeholder = NSLocalizedString("New password", comment: "")
        confPassTxt.placeholder = NSLocalizedString("Confirm password", comment: "")
        aboutMeTxt.delegate = self
        
        self.user = user

        fillData()
        saveBtn.style()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updateChangePassView(animated: true)
    }
    
    override func viewDidLayoutSubviews() {
        imageContainer.setCircular()
        imageContainer.lightBorder()
    }
    
    func fillData() {
        profileIV.downloadWithLoader(url: user.image, color: UIColor.lightGray, largeLoader: false)
        nameTxt.text = user.name
        aboutMeTxt.text = user.summary
        resizeAboutMeTxt()
    }
    
    func updateChangePassView(animated: Bool) {
        UIView.animate(withDuration: animated ? 0.25 : 0) {
            self.editPassContainer.isHidden = !self.isChangingPass
            self.editPassContainer.alpha = self.isChangingPass ? 1 : 0
            self.changePassBtn.setTitle(self.isChangingPass ? NSLocalizedString("Cancel", comment: "") : NSLocalizedString("Change password", comment: ""), for: .normal)
        }
    }
    
    func validate() {
        var message: String?
        var params: JSON = [:]
        
        if let name = nameTxt.text, !name.isEmpty {
            params.updateValue(name, forKey: "name")
        } else {
            message = NSLocalizedString("Please enter a name", comment: "")
        }
        
        if let desc = aboutMeTxt.text, !desc.isEmpty {
            params.updateValue(desc, forKey: "description")
        }
        
        if let image = newImage?.base64() {
            params.updateValue(image, forKey: "image")
        }
        
        if isChangingPass {
            if let currentPass = currPassTxt.text, !currentPass.isEmpty {
                if let pass = passTxt.text, !pass.isEmpty {
                    if let confPass = confPassTxt.text, !confPass.isEmpty {
                        if pass == confPass {
                            params.updateValue(currentPass, forKey: "last_password")
                            params.updateValue(pass, forKey: "password")
                            params.updateValue(confPass, forKey: "confirm_password")
                        } else {
                            message = NSLocalizedString("The passwords doesn't match.", comment: "")
                        }
                    } else {
                        message = NSLocalizedString("Please confirm the new password", comment: "")
                    }
                } else {
                    message = NSLocalizedString("Please enter the new password", comment: "")
                }
            } else {
                message = NSLocalizedString("Please enter your current password", comment: "")
            }
        }
        if let message = message {
            AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
            return
        }
        
        updateProfile(params: params)
    }
    
    @IBAction func pickPhoto(_ sender: Any) {
        ImagePicker.pickPhoto(vc: self)
    }
    
    @IBAction func changePass(_ sender: Any) {
        isChangingPass = !isChangingPass
        updateChangePassView(animated: true)
    }
    
    @IBAction func saveChanges(_ sender: Any) {
        validate()
    }
}

//MARK: - TEXTVIEW
extension EditProfileVC: UITextViewDelegate {
    
    func resizeAboutMeTxt() {
        let sizeThatFits: CGSize = aboutMeTxt.sizeThatFits(CGSize(width: aboutMeTxt.frame.width, height: CGFloat(MAXFLOAT)))
        if sizeThatFits.height < 40 { return }
        aboutMeHeight.constant = sizeThatFits.height
    }
    
    func textViewDidChange(_ textView: UITextView) {
        guard textView == aboutMeTxt else { return }
        resizeAboutMeTxt()
    }
}

// MARK: - IMAGEPICKER
extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else { print ("error") }
        
        if let image = newImage {
            profileIV.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("image picker cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - REQUESTS
extension EditProfileVC {
    
    func updateProfile(params: JSON) {
        self.view.endEditing(true)
        APIClient().request(request: .UpdateProfile(vc: self, params: params)) { [weak self] (response, success) in
            if success, let `self` = self {
                guard let userDic = response.data["user"] as? JSON,
                    let user = try? User(userDic) else { self.showServerBadErrorResponse(); return }
                user.saveToDefaults()
                self.navigationController?.popViewController(animated: true, completion: self.didUpdate)
            }
        }
    }
}
