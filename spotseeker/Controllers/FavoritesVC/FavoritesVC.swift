//
//  FavoritesVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 15/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation

class FavoritesVC: SpotListViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Favorites", comment: "")
    }
    
    override func getSpots() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        var params: JSON = [:]
        
        if var location = userLocation {
            if let locationSent = locationSent {
                location = locationSent
            }
            params.updateValue(location.latitude, forKey: "lat")
            params.updateValue(location.longitude, forKey: "lng")
            
            locationSent = location
        } else {
            params.removeValue(forKey: "lat")
            params.removeValue(forKey: "lng")
        }
        let request = ApiRequest.UserFavorites(vc: self, params: params, page: page)
        self.getSpots(request: request)
    }
}
