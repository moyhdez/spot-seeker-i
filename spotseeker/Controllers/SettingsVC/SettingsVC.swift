//
//  SettingsVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    @IBOutlet weak var privacyPolicyLbl: UILabel!
    @IBOutlet weak var termsAndConditionsLbl: UILabel!
    @IBOutlet weak var aboutLbl: UILabel!
    @IBOutlet weak var logoutLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Settings", comment: "")
        privacyPolicyLbl.text = NSLocalizedString("Privacy policy", comment: "")
        termsAndConditionsLbl.text = NSLocalizedString("Terms and conditions", comment: "")
        aboutLbl.text = NSLocalizedString("About SpotSeeker", comment: "")
        logoutLbl.text = NSLocalizedString("Log out", comment: "")
    }
    
    @IBAction func termsAndConditions(_ sender: Any) {
    }
    
    @IBAction func privacyPolicy(_ sender: Any) {
    }
    
    @IBAction func about(_ sender: Any) {
    }
    
    @IBAction func logout(_ sender: Any) {
        AppDelegate.logout(vc: self)
    }

}
