//
//  HomeVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class HomeVC: SpotListViewController {
    
    //MAPS API KEY
    //AIzaSyDcIfZmF_LxB-yRuDgkIVnYFOc9iy0IuLc
    
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet weak var eventsCollectionView: UICollectionView!
    @IBOutlet weak var promosCollectionView: UICollectionView!
    @IBOutlet weak var eventsView: UIView!
    @IBOutlet weak var promosView: UIView!
    @IBOutlet weak var eventsLbl: UILabel!
    @IBOutlet weak var promosLbl: UILabel!
    
    var isInMap = false
    
    var maxDistance: String?
    
    var mapSpots = [(Spot, GMSMarker?)]() {
        didSet {
            addMapMarkers()
        }
    }
    
    var events: [Event] = [] {
        didSet {
            UIView.animate(withDuration: 0.3, animations: {
                self.eventsView.visibilityWithAlpha(isHidden: self.events.isEmpty)
                self.tableView.relayoutTableHeaderView()
            }) { (_) in
                self.eventsCollectionView.collectionViewLayout.invalidateLayout()
                self.eventsCollectionView.reloadData()
            }
        }
    }
    
    var promos: [Promotion] = [] {
        didSet {
            UIView.animate(withDuration: 0.3, animations: {
                self.promosView.visibilityWithAlpha(isHidden: self.promos.isEmpty)
                self.tableView.relayoutTableHeaderView()
            }) { (_) in
                self.promosCollectionView.collectionViewLayout.invalidateLayout()
                self.promosCollectionView.reloadData()
            }
        }
    }
    
    var isLoadingPromos = false
    var paginationPromos: Pagination?
    var isLoadingEvents = false
    var paginationEvents: Pagination?
    
    var params: JSON = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "SPOT SEEKER"
        let itemRight1 = UIBarButtonItem(image: #imageLiteral(resourceName: "feed"), style: .plain, target: self, action: #selector(goToFeed))
        let itemRight2 = UIBarButtonItem(image: #imageLiteral(resourceName: "filter"), style: .plain, target: self, action: #selector(filter))
        self.navigationItem.setRightBarButtonItems([itemRight2, itemRight1], animated: true)
        self.navigationItem.setLeftBarButton(UIBarButtonItem(image: #imageLiteral(resourceName: "puntero_inactive"), style: .plain, target: self, action: #selector(spotsViews)), animated: true)
        
        self.mapView.delegate = self
        self.mapView.settings.myLocationButton = true
        self.mapView.isMyLocationEnabled = true
        
        firstTimeLocationUpdate = {
            if let userLocation = self.userLocation {
                self.animateCameraTo(userLocation)
            }
        }
        if #available(iOS 11.0, *) {
        } else {
            self.tableView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
        }
        
        setUpPromosAndEvents()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.relayoutTableHeaderView()
    }
    
    func setUpPromosAndEvents() {
        setUp(eventsCollectionView, cellType: EventCell.self)
        setUp(promosCollectionView, cellType: PromoCell.self)
        
        setSelectedScreenViews()
        
        eventsView.visibilityWithAlpha(isHidden: events.isEmpty)
        promosView.visibilityWithAlpha(isHidden: promos.isEmpty)
        
        self.onRefresh = {
            self.paginationPromos = nil
            self.paginationEvents = nil
            self.getEvents()
            self.getPromos()
        }
        
        getPromos()
        getEvents()
    }
    
    func setUp<T: UICollectionViewCell>(_ collectionView: UICollectionView, cellType: T.Type) {
        collectionView.register(cellType)
        collectionView.delegate = self
        collectionView.dataSource = self
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .always
        }
    }
    
    func setSelectedScreenViews() {
        self.tableView.isHidden = self.isInMap
        self.tableView.alpha = self.isInMap ? 0 : 1
        self.mapView.isHidden = !self.isInMap
        self.mapView.alpha = !self.isInMap ? 0 : 1
        self.navigationItem.leftBarButtonItem?.image = self.isInMap ? #imageLiteral(resourceName: "list") : #imageLiteral(resourceName: "puntero_inactive")
    }
    
    func clearAndGetMapSpots() {
        mapView.clear()
        mapSpots = []
        getMapSpots(mapView)
    }
    
    func addMapMarkers() {
        for i in (0..<mapSpots.count) {
            if mapSpots[i].1 != nil { continue }
            let spot = mapSpots[i].0
            guard let location = spot.coordinates() else { continue }
            let marker = GMSMarker(position: location)
            marker.map = mapView
            marker.userData = spot
            mapSpots[i] = (spot, marker)
            guard let image = spot.category?.image else { return }
            UIImage.download(url: image, completion: { (image) in
                if let image = image {
                    marker.icon = image.resizeImage(maxSize: 50)
                    marker.map = self.mapView
                }
            })
        }
    }
    
    @objc func goToFeed() {
        let vc = FeedVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func spotsViews() {
        isInMap = !isInMap
        UIView.animate(withDuration: 0.3) {
            self.setSelectedScreenViews()
        }
    }
    
    @objc func filter() {
        let filterVC = FilterVC.instance()
        filterVC.homeVC = self
        self.navigationController?.pushViewController(filterVC, animated: true)
    }
    
    override func getSpots() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        if var location = userLocation {
            if let locationSent = locationSent {
                location = locationSent
            }
            params.updateValue(location.latitude, forKey: "lat")
            params.updateValue(location.longitude, forKey: "lng")
            //            params.updateValue("50", forKey: "max_distance")
            locationSent = location
        } else {
            params.removeValue(forKey: "lat")
            params.removeValue(forKey: "lng")
        }
        
        if let distance = maxDistance {
            params.updateValue(distance, forKey: "max_distance")
        } else {
            params.removeValue(forKey: "max_distance")
        }
        let request = ApiRequest.SpotsFilter(vc: self, params: params, page: page)
        self.getSpots(request: request)
    }
}

//MARK: - Map functions
extension HomeVC {
    
    func animateCameraTo(_ location: CLLocationCoordinate2D) {
        if mapView == nil { return }
        let camera = GMSCameraPosition.camera(withTarget: location, zoom: 16)
        mapView.animate(to: camera)
    }
}

extension HomeVC: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        getMapSpots(mapView)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        guard let spot = marker.userData as? Spot else { return true }
        let vc = DetailSpotVC.instance()
        vc.spot = spot
        self.navigationController?.pushViewController(vc, animated: true)
        return true
    }
}

//MARK: - COLLECTION VIEW
extension HomeVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == eventsCollectionView {
            return events.count
        } else if collectionView == promosCollectionView {
            return promos.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == eventsCollectionView {
            let cell = collectionView.dequeueReusableCell(for: indexPath) as EventCell
            cell.set(for: events[indexPath.row])
            return cell
        } else if collectionView == promosCollectionView {
            let cell = collectionView.dequeueReusableCell(for: indexPath) as PromoCell
            cell.set(for: promos[indexPath.row])
            return cell
            
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == eventsCollectionView {
            guard let pagination = paginationEvents else { return }
            if indexPath.row == events.count - 1 && pagination.currentPage < pagination.lastPage && !isLoadingEvents {
                getEvents()
            }
        } else if collectionView == promosCollectionView {
            guard let pagination = paginationPromos else { return }
            if indexPath.row == promos.count - 1 && pagination.currentPage < pagination.lastPage && !isLoadingPromos {
                getPromos()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == eventsCollectionView, let url = events[indexPath.row].link {
            AppDelegate.open(url)
        } else if collectionView == promosCollectionView, let url = promos[indexPath.row].link {
            AppDelegate.open(url)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = collectionView.frame.height - 2
        return CGSize(width: collectionView == eventsCollectionView ? side * 1.6 : side - 35, height: side)
    }
}

// MARK: - Requests
extension HomeVC {
    
    func getMapSpots(_ mapView: GMSMapView) {
        let ids = mapSpots.map({"\($0.0.id ?? 0)"}).joined(separator: ",")
        let distance = ceil(mapView.getRadius()/1000)
        params.updateValue(distance, forKey: "max_distance")
        params.updateValue(ids, forKey: "spots_ids")
        params.updateValue(mapView.camera.target.latitude, forKey: "lat")
        params.updateValue(mapView.camera.target.longitude, forKey: "lng")
        
        APIClient().request(request: .MapSpots(vc: self, params: params)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success {
                guard let spotsData = response.data["spots"] as? [JSON],
                    let spots = try? spotsData.compactMap(Spot.init) else { return }
                
                self.mapSpots.append(contentsOf: spots.map({($0, nil)}))
            }
        }
    }
    
    func getEvents() {
        isLoadingEvents = true
        APIClient().request(request: .GetEvents(vc: self)) { [weak self] (response, success) in
            self?.isLoadingEvents = false
            guard let `self` = self else { return }
            if success {
                guard let eventsData = response.data["events"] as? [JSON],
                    let paginationData = response.data["pagination"] as? JSON,
                    let events = try? eventsData.compactMap(Event.init),
                    let  pagination = try? Pagination(paginationData) else { self.showServerBadErrorResponse(); return }
                
                if self.pagination?.currentPage ?? 1 == 1 {
                    self.events = events
                } else {
                    self.events.append(contentsOf: events)
                }
                
                self.paginationEvents = pagination
            }
        }
    }
    
    func getPromos() {
        isLoadingPromos = true
        APIClient().request(request: .GetPromos(vc: self)) { [weak self] (response, success) in
            self?.isLoadingPromos = false
            guard let `self` = self else { return }
            if success {
                guard let promosData = response.data["promotions"] as? [JSON],
                    let paginationData = response.data["pagination"] as? JSON,
                    let promos = try? promosData.compactMap(Promotion.init),
                    let  pagination = try? Pagination(paginationData) else { self.showServerBadErrorResponse(); return }
                
                
                if self.pagination?.currentPage ?? 1 == 1 {
                    self.promos = promos
                } else {
                    self.promos.append(contentsOf: promos)
                }
                
                self.paginationPromos = pagination
            }
        }
    }
}
