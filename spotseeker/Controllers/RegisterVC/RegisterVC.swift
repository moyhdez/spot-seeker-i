//
//  RegisterVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 18/02/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit
import TextFieldEffects

class RegisterVC: UIViewController {
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var profileIV: UIImageView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var facebookBtn: UIButton!
    @IBOutlet weak var emailTxt: HoshiTextField!
    @IBOutlet weak var nameTxt: HoshiTextField!
    @IBOutlet weak var passTxt: HoshiTextField!
    @IBOutlet weak var confPassTxt: HoshiTextField!

    var newImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Sign up screen title", comment: "")
        doneBtn.setTitle(NSLocalizedString("Sign up", comment: ""), for: .normal)
        facebookBtn.setTitle(NSLocalizedString("Signup by Facebook", comment: ""), for: .normal)
        nameTxt.placeholder = NSLocalizedString("Name", comment: "")
        emailTxt.placeholder = NSLocalizedString("Email", comment: "")
        passTxt.placeholder = NSLocalizedString("Password", comment: "")
        confPassTxt.placeholder = NSLocalizedString("Confirm password", comment: "")
        doneBtn.style()
        facebookBtn.style()
    }
    
    override func viewDidLayoutSubviews() {
        imageContainer.setCornerRadius(1)
    }
    
    @IBAction func pickPhoto(_ sender: Any) {
        ImagePicker.pickPhoto(vc: self)
    }
    
    @objc func validate() {
        
        self.view.endEditing(true)
        
        var message = ""
        
        if emailTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter your email", comment: "")
        } else if nameTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter a name", comment: "")
        } else if passTxt.text!.isEmpty {
            message = NSLocalizedString("Please enter the new password", comment: "")
        } else if confPassTxt.text!.isEmpty {
            message = NSLocalizedString("Please confirm the new password", comment: "")
        } else if passTxt.text != confPassTxt.text {
            message = NSLocalizedString("The passwords doesn't match", comment: "")
        } else {
            var params: JSON = [
                "email": emailTxt.text!,
                "name": nameTxt.text!,
                "password": passTxt.text!,
                "password_confirmation": confPassTxt.text!
            ]
            
            if let image = newImage?.base64() {
                params.updateValue(image, forKey: "image")
            }
            
            register(params: params)
            return
        }
        
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Missing data", comment: ""), message: message, OKButtonTitle: NSLocalizedString("Ok", comment: ""))
    }
    
    @IBAction func register(_ sender: Any) {
        self.validate()
    }

}

// MARK: - IMAGEPICKER
extension RegisterVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            newImage = image.resizeImage(maxSize: 1000)
        } else { print ("error") }
        
        if let image = newImage {
            profileIV.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("image picker cancel")
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - REQUESTS
extension RegisterVC {
    
    func register(params: JSON) {
        self.view.endEditing(true)
        APIClient().request(request: .Register(vc: self, params: params)) { [weak self] (response, success) in
            if success, let `self` = self {
                guard let userDic = response.data["user"] as? JSON,
                        let user = try? User(userDic )else { self.showServerBadErrorResponse(); return }
                
                user.saveToDefaults()
                
                self.login()
            }
        }
    }
    
    func login() {
        let params: JSON = [
            "username": emailTxt.text ?? "",
            "password": passTxt.text ?? ""
        ]
        
        APIClient().request(request: .LoginByEmail(vc: self, params: params)) { [weak self] (response ,success) in
            if success {
                guard let userDic = response.data["user"] as? JSON
                    , let user = try? User(userDic), let oauth = response.data["oauth"] as? JSON else { return }
                
                user.saveToDefaults()
                UserDefaults.standard.setValue(oauth["access_token"] as? String, forKey: K.Defaults.AccessToken)
                UserDefaults.standard.setValue(oauth["refresh_token"] as? String, forKey: K.Defaults.RefreshToken)
                if let expiresIn = oauth["expires_in"] as? Double {
                    UserDefaults.standard.setValue(Date().addingTimeInterval(expiresIn).toStringWith(format: K.Defaults.DateFormat), forKey: K.Defaults.TokenExpiresAt)
                }
                
                let vc = MainTabBarController()
                AppDelegate.sharedInstance().switchRootViewController(vc: vc, animated: true)
            } else {
                AlertsController.showAlert(delegate: self, title: "Error", message: NSLocalizedString("Registration was successful but there was an error while trying to login.", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), success: {
                    self?.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
}
