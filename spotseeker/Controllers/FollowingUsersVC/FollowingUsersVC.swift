//
//  FollowingUsersVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 20/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class FollowingUsersVC: UITableViewController {
    
    var user: User!
    
    var users: [User] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var pagination: Pagination?
    var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("Following", comment: "")
        
        tableView.register(UserSearchCell.self)
        tableView.register(EmptyStateCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl!)
        }
        getFollowing()
    }
    
    @objc func refresh(sender: AnyObject) {
        self.pagination = nil
        getFollowing()
    }
    
}

// MARK: - TableView
extension FollowingUsersVC {
    
    @objc func followUser(_ sender: UIButton) {
        let row = sender.tag
        self.follow(users[row]) { success, following in
            if success {
                if !following && self.user.id == User.getFromDefaults()?.id ?? -1 {
                    self.users.remove(at: row)
                } else {
                    self.users[row].following = following
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func userCellTapped(at index: IndexPath) {
        self.goToProfile(of: users[index.row])
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if users.isEmpty { return 1 }
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if users.isEmpty { return tableView.dequeueReusableCell(for: indexPath) as EmptyStateCell }
        let cell =  tableView.dequeueReusableCell(for: indexPath) as UserSearchCell
        cell.addUserBtn.tag = indexPath.row
        cell.addUserBtn.addTarget(self, action: #selector(followUser(_:)), for: .touchUpInside)
        cell.set(for: users[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if users.isEmpty { return }
        userCellTapped(at: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let pagination = pagination else { return }
        if indexPath.row == users.count - 1 && pagination.currentPage < pagination.lastPage && !isLoading{
            getFollowing()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if users.isEmpty { return UITableView.automaticDimension }
        return 60
    }
}

extension FollowingUsersVC {
    
    func getFollowing() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        isLoading = true
        APIClient().request(request: .Following(vc: self, user: user, page: page)) { [weak self] (response, success) in
            self?.isLoading = false
            guard let `self` = self else { return }
            if success {
                guard let usersData = response.data["users"] as? [JSON],
                    let paginationData = response.data["pagination"] as? JSON,
                    let  pagination = try? Pagination(paginationData),
                    let users = try? usersData.compactMap(User.init) else { self.showServerBadErrorResponse(); return }
                
                self.pagination = pagination
                
                if self.pagination?.currentPage ?? 1 == 1 {
                    self.users = []
                }
                
                self.users.append(contentsOf: users)
            }
        }
    }
}
