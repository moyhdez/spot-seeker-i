//
//  FeedVC.swift
//  spotfinder
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class FeedVC: UITableViewController {
    
    var pagination: Pagination?
    var isLoading = false
    
    var feedPosts = [FeedPost]() {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Feed", comment: "")
        
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableView.automaticDimension
        tableView.register(FeedCell.self)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl!)
        }
        if #available(iOS 11.0, *) {
        } else {
            self.tableView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
        }
        getPosts()
    }
    
    @objc func refresh(sender: AnyObject) {
        clearAndGetPosts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let view = UIView()
        view.backgroundColor = .red
        self.view.addSubview(view)
        view.fillSuperview()
    }
    
    func clearAndGetPosts() {
        pagination = nil
        getPosts()
    }
    
}

// MARK: - TableView
extension FeedVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedPosts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(for: indexPath) as FeedCell
        cell.set(for: feedPosts[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let pagination = pagination else { return }
        if indexPath.row == feedPosts.count - 1 && pagination.currentPage < pagination.lastPage && !isLoading{
            getPosts()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = FeedDetailVC.instance()
        vc.post = feedPosts[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - Requests
extension FeedVC {
    
    func getPosts() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        isLoading = true
        APIClient().request(request: .GetFeed(vc: self, page: page)) { [weak self] (response, success) in
            self?.isLoading = false
            guard let `self` = self else { return }
            if success {
                guard let feedPostsData = response.data["articles"] as? [JSON]
                    , let paginationData = response.data["pagination"] as? JSON,
                    let feedPosts = try? feedPostsData.compactMap(FeedPost.init),
                    let  pagination = try? Pagination(paginationData) else { self.showServerBadErrorResponse(); return }
                
                self.pagination = pagination
                
                if self.pagination?.currentPage ?? 1 == 1 {
                    self.feedPosts = []
                }
                
                self.feedPosts.append(contentsOf: feedPosts)
            }
        }
    }
}
