//
//  ProfileVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 24/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation


class ProfileVC: SpotListViewController {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var summaryLbl: UILabel!
    @IBOutlet weak var spotsLbl: UILabel!
    @IBOutlet weak var followersLbl: UILabel!
    @IBOutlet weak var followingLbl: UILabel!
    @IBOutlet weak var spotsCountLbl: UILabel!
    @IBOutlet weak var followersCountLbl: UILabel!
    @IBOutlet weak var followingCountLbl: UILabel!
    @IBOutlet weak var followBtn: UIButton!
    
    var user: User!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        followersLbl.text = NSLocalizedString("Followers", comment: "")
        followingLbl.text = NSLocalizedString("Following", comment: "")
        self.navigationItem.title = NSLocalizedString("Profile", comment: "")
        
        if user == nil {
            guard let user = User.getFromDefaults() else {
                self.navigationController?.tabBarController?.selectedIndex = 0
                return
            }
            
            self.user = user
        }
        self.onRefresh = {
            self.getProfile()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.relayoutTableHeaderView()
        profileImg.setCircular()
        profileImg.lightBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fillData()
        let loggedUser = User.getFromDefaults()
        followBtn.visibilityWithAlpha(isHidden: user.id == loggedUser?.id ?? -1)
        if user.id == loggedUser?.id ?? -1 {
            let item = UIBarButtonItem(image:#imageLiteral(resourceName: "notification"), style: .plain, target: self, action: #selector(notifications))
            let item1 = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(editProfile))
            let item2 = UIBarButtonItem(image: #imageLiteral(resourceName: "settings_unfilled"), style: .plain, target: self, action: #selector(settings))
            self.navigationItem.setRightBarButtonItems([item2, item1, item], animated: true)
        }
    }
    
    func fillData() {
        profileImg.downloadWithLoader(url: user.image, color: UIColor.lightGray, largeLoader: false)
        summaryLbl.text = user.summary
        nameLbl.text = user.name
        followersCountLbl.text = "\(user.followersCount ?? 0)"
        followingCountLbl.text = "\(user.followingCount ?? 0)"
        spotsCountLbl.text = "\(user.spotsCount ?? 0)"
        followBtn.setImage(user.following ? #imageLiteral(resourceName: "follower") : #imageLiteral(resourceName: "adduser"), for: .normal)
    }
    
    @objc func notifications() {
        let vc = NotificationsVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func settings() {
        let vc = SettingsVC.instance()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func editProfile() {
        let vc = EditProfileVC.instance()
        vc.didUpdate = {
            guard let user = User.getFromDefaults() else {
                self.navigationController?.tabBarController?.selectedIndex = 0
                return
            }
            
            self.user = user

            self.fillData()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func getSpots() {
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        let request = ApiRequest.SpotsFilter(vc: self, params: ["user_id": user.id], page: page)
        self.getSpots(request: request)
    }
    
    @IBAction func followUser(_ sender: Any) {
        self.follow(user) { (success, following) in
            if success {
                self.user.following = following
                self.user.followersCount = (self.user.followersCount ?? 0) + (following ? 1 : -1)
                self.fillData()
            }
        }
    }
    
    @IBAction func followers(_ sender: Any) {
        let vc = FollowersVC.instance()
        vc.user = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func following(_ sender: Any) {
        let vc = FollowingUsersVC.instance()
        vc.user = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ProfileVC {
    
    func getProfile() {
        APIClient().request(request: .GetUser(vc: self, id: user.id)) { [weak self] (response, success) in
            if success, let `self` = self {
                guard let userDic = response.data["user"] as? JSON
                    , let user = try? User(userDic) else { self.showServerBadErrorResponse(); return }
                if user.id == User.getFromDefaults()?.id ?? -1 {
                    user.saveToDefaults()
                }
                self.user.clone(with: user)
                self.fillData()
            }
        }
    }
}

