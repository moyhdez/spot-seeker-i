//
//  DetailSpotVC.swift
//  spotseeker
//
//  Created by Moy Hdez on 21/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import IQKeyboardManagerSwift
import ImageSlideshow
import AVFoundation
import AVKit

class DetailSpotVC: UIViewController, RefreshableViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotDescription: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var addUserBtn: UIButton!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var sendCommentBtn: UIButton!
    @IBOutlet weak var commentTxt: UITextView!
    @IBOutlet weak var commentTxtHeight: NSLayoutConstraint!
    @IBOutlet weak var commentLoader: UIActivityIndicatorView!
    @IBOutlet weak var difficultyStack: UIStackView!
    @IBOutlet weak var commentBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var meetingPointBtn: UIButton!
    @IBOutlet weak var followUserBtn: UIButton!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    
    var spot: Spot!
    
    var refreshControl: UIRefreshControl!
    var pagination: Pagination?
    var isLoading = false
    
    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D()
    
    let commentTxtPlaceholder = NSLocalizedString("Leave a comment...", comment: "")
    let commentTxtPlaceholderColor = #colorLiteral(red: 0.7233663201, green: 0.7233663201, blue: 0.7233663201, alpha: 1)
    
    var isLoadingFav = false
    
    var favItem: UIBarButtonItem {
        get {
            return .init(image: spot.favorite ? #imageLiteral(resourceName: "fav_active") : #imageLiteral(resourceName: "fav"), style: .plain, target: self, action: #selector(addToFavs))
        }
    }
    
    var barItems: [UIBarButtonItem] {
        get {
            var items: [UIBarButtonItem] = []
            if User.getFromDefaults()?.id == spot.user?.id {
                let item2 = UIBarButtonItem(image: #imageLiteral(resourceName: "trash_icon"), style: .plain, target: self, action: #selector(deletePressed))
                let item1 = UIBarButtonItem(image: #imageLiteral(resourceName: "edit"), style: .plain, target: self, action: #selector(editPressed))
                items = [item1, item2, favItem]
            } else {
                items = [favItem]
            }
            
            return items
        }
    }
    
    var footer: UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 20))
        footerView.backgroundColor = UIColor.clear
        let activityIndicator = UIActivityIndicatorView(frame: footerView.frame)
        activityIndicator.style = .gray
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        activityIndicator.tag = 1
        footerView.addSubview(activityIndicator)
        return footerView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        }
        
        followUserBtn.visibilityWithAlpha(isHidden: User.getFromDefaults()?.id ?? -1 == spot.user?.id ?? 0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShowing), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHidding), name: UIResponder.keyboardWillHideNotification, object: nil)
    
        setUpLocation()
        initViews()
        loadData()
        getComments()
        tableView.tableFooterView = footer
        updateTableViewHeader()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //View jumping on push because the tabbar
        if let constraint = self.view.constraints.filter({$0.identifier == "bottomConstraint" }).first {
            self.view.removeConstraint(constraint)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        updateTableViewHeader()
    }
    
    func updateTableViewHeader() {
        if let headerView = self.tableView.tableHeaderView {
            self.tableView.setTableHeaderView(headerView: headerView)
            self.tableView.updateHeaderViewFrame()
        }
    }
    
    func initViews() {
        initTableView()
        clear(commentTxt)
        commentTxt.setCornerRadius(3)
        commentTxt.delegate = self
        userIV.setCircular()
        userIV.lightBorder()
        meetingPointBtn.setCircular()
        meetingPointBtn.lightBorder()
        meetingPointBtn.addShadow()
        slideshow.layer.cornerRadius = 1
        containerView.layer.cornerRadius = 1
        commentLoader.visibilityWithAlpha(isHidden: true)
        slideshow.contentScaleMode = .scaleAspectFill
        let slideShowTGR = UITapGestureRecognizer(target: self, action: #selector(self.didTapImageSlideShow))
        slideshow.addGestureRecognizer(slideShowTGR)
        initCollectionView()
    }
    
    func initCollectionView() {
        videosCollectionView.register(VideoCell.self)
        videosCollectionView.delegate = self
        videosCollectionView.dataSource = self
        if let layout = videosCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
        }
        if #available(iOS 11.0, *) {
            videosCollectionView.contentInsetAdjustmentBehavior = .always
        }
    }
    
    func initTableView() {
        tableView.estimatedRowHeight = 100
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = .main
        refreshControl.addTarget(self, action:#selector(self.refreshSpot), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        if #available(iOS 11.0, *) {
        } else {
            self.tableView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
        }
    }
    
    func setUpLocation() {
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func setFavItem() {
        self.navigationItem.setRightBarButtonItems(barItems, animated: true)
    }
    
    func loadData() {
        spotDescription.text = spot.summary
        spotTitle.text = spot.title
        categoryLbl.text = spot.category?.title
        userName.text = spot.user?.name
        setFavItem()
        videosCollectionView.visibilityWithAlpha(isHidden: spot.videos.isEmpty)
        followUserBtn.setImage(spot.user?.following ?? false ? #imageLiteral(resourceName: "follower") : #imageLiteral(resourceName: "adduser"), for: .normal)
        UIView.animate(withDuration: 0.1) {
            self.view.layoutSubviews()
        }
        
        for (i, view) in difficultyStack.arrangedSubviews.enumerated() {
            if let imgView = view as? UIImageView {
                imgView.image = spot.difficulty ?? 5 > i ? #imageLiteral(resourceName: "wheel active") : #imageLiteral(resourceName: "wheel")
            }
        }
        
        //set images
        slideshow.activityIndicator = DefaultActivityIndicator(style: .gray, color: nil)
        let sources = spot.images.compactMap { AlamofireSource(urlString: $0) }
        slideshow.setImageInputs(sources)
        
        if let image = spot.user?.image {
            userIV.downloadWithLoader(url: image, color: UIColor.lightGray, largeLoader: false)
        }
    }
    
    func stopLoading() {
        isLoading = false
        UIView.animate(withDuration: 0.4) {
            (self.tableView.tableFooterView?.viewWithTag(1) as? UIActivityIndicatorView)?.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    func startLoading() {
        isLoading = true
        (self.tableView.tableFooterView?.viewWithTag(1) as? UIActivityIndicatorView)?.startAnimating()
    }
    
    func validate(comment: String?) -> Bool {
        guard let comment = comment else { return false }
        return !comment.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    @objc func handleKeyboardShowing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            commentBottomConstraint.constant = keyboardSize.height
        }
        self.view.layoutIfNeeded()
        self.tableView.scrollToBottom()
    }
    
    @objc func handleKeyboardHidding(notification: Notification) {
        commentBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
        self.tableView.scrollToBottom()
    }
    
    @objc func didTapImageSlideShow() {
        slideshow.presentFullScreenController(from: self)
    }
    
    @objc func addToFavs() {
        if isLoadingFav { return }
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        if !AppDelegate.checkUser(self) { return }
        self.isLoadingFav = true
        addToFavorites()
    }
    
    @objc func deletePressed() {
        guard User.getFromDefaults()?.id == spot.user?.id else {
            return
        }
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Delete spot", comment: ""), message: NSLocalizedString("Are you sure that you want to delete this spot?", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), otherButtonTitle: NSLocalizedString("Cancel", comment: ""), success: {
            if #available(iOS 10.0, *) {
                UIImpactFeedbackGenerator().impactOccurred()
            }
            self.deleteSpot()
        })
    }
    
    @objc func editPressed() {
        guard User.getFromDefaults()?.id == spot.user?.id else {
            return
        }
        let vc = CreateSpotVC.instance()
        vc.spot = spot
        vc.didEdit = { spot in
            self.spot.clone(with: spot)
            self.loadData()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteCommentPressed(at index: Int) {
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        AlertsController.showAlert(delegate: self, title: NSLocalizedString("Delete comment", comment: ""), message: NSLocalizedString("Are you sure that you want to delete this comment?", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), otherButtonTitle: NSLocalizedString("Cancel", comment: ""), success: {
            if #available(iOS 10.0, *) {
                UIImpactFeedbackGenerator().impactOccurred()
            }
            self.deleteComment(at: index)
        })
    }
    
    @IBAction func distanceBtnPressed(_ sender: Any) {
        if let coordinates = spot.coordinates() {
            AppDelegate.openMap(self, with: coordinates)
        }
    }
    
    @IBAction func sendComment(_ sender: Any) {
        let comment = commentTxt.text.trimmingCharacters(in: .whitespacesAndNewlines)
        if validate(comment: comment) {
            send(comment: comment)
        }
    }
    
    @IBAction func meetingPointPressed(_ sender: Any) {
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        AlertsController.showAlert(delegate: self, message: NSLocalizedString("Your followers will be notified that you are in this spot", comment: ""), OKButtonTitle: NSLocalizedString("Ok", comment: ""), otherButtonTitle: NSLocalizedString("Cancel", comment: ""), success: {
            if #available(iOS 10.0, *) {
                UIImpactFeedbackGenerator().impactOccurred()
            }
            self.checkIn()
        }, cancel: nil)
    }
    
    @IBAction func followUser(_ sender: Any) {
        guard let user = spot.user, user.id != User.getFromDefaults()?.id ?? -1 else { return }
        self.follow(user) { success, following in
            if success {
                self.spot.user?.following = following
                self.loadData()
            }
        }
    }
    
    @IBAction func goToUserProfile(_ sender: Any) {
        guard let user = spot.user else { return }
        self.goToProfile(of: user)
    }
}

// MARK: - TableView
extension DetailSpotVC: UITableViewDelegate, UITableViewDataSource {
    
    @objc func goToCommentUserProfile(_ sender: UIButton) {
        let user = spot.comments[sender.tag].user
        self.goToProfile(of: user)
    }
    
    @objc func showCommentOptions(_ sender: UIButton) {
        let index = sender.tag
        let user = spot.comments[index].user
        guard user.id == User.getFromDefaults()?.id ?? -1 else { return }
        AlertsController.showDeleteActionSheet(delegate: self) { (delete) in
            self.deleteCommentPressed(at: index)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spot.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(for: indexPath) as CommentCell
        cell.userBtn.tag = indexPath.row
        cell.userBtn.addTarget(self, action: #selector(goToCommentUserProfile(_:)), for: .touchUpInside)
        cell.optionsBtn.tag = indexPath.row
        cell.optionsBtn.addTarget(self, action: #selector(showCommentOptions(_:)), for: .touchUpInside)
        cell.setUp(for: spot.comments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let pagination = pagination else { return }
        if indexPath.row == spot.comments.count - 1 && pagination.currentPage < pagination.lastPage && !isLoading{
            getComments()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK: - COLLECTION VIEW
extension DetailSpotVC: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return spot.videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as VideoCell
        cell.set(for: spot.videos[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let url = URL(string: spot.videos[indexPath.row]) {
            let player = AVPlayer(url: url)
            let vc = AVPlayerViewController()
            try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try? AVAudioSession.sharedInstance().setActive(true)
            vc.player = player
            
            present(vc, animated: true) {
                vc.player?.play()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = collectionView.frame.height - 2
        return CGSize(width: side, height: side)
    }
}

// MARK: - Textfield
extension DetailSpotVC: UITextViewDelegate {
    
    func adjustTextViewSize() {
        let contentSize = commentTxt.contentSize
        if contentSize.height >= 33 && contentSize.height <= 75 {
            commentTxtHeight.constant = contentSize.height
        }
    }
    
    func textViewDidChange(_ textView: UITextView){
        adjustTextViewSize()
        sendCommentBtn.isEnabled = validate(comment: textView.text)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.text.count > 500 && range.length == 0 {
            return false
        }
    
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == commentTxtPlaceholderColor{
            textView.text = nil
            textView.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty { clear(textView) }
    }
    
    func clear(_ textView: UITextView) {
        textView.text = commentTxtPlaceholder
        textView.textColor = commentTxtPlaceholderColor
        sendCommentBtn.isEnabled = false
        UIView.animate(withDuration: 0.3) {
            self.adjustTextViewSize()
        }
    }
}

//MARK: - Location manager
extension DetailSpotVC: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while updating location " + error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            if userLocation.latitude != location.coordinate.latitude && userLocation.longitude != location.coordinate.longitude{
                userLocation = location.coordinate
                distanceLbl.text = spot.coordinates()?.readableDistance(to: userLocation)
            }
        }
    }
}

// MARK: - Requests
extension DetailSpotVC {
    
    @objc func refreshSpot() {
        guard let id = spot.id else { return }
        APIClient().request(request: .Spot(vc: self, id: id)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success, let spotJSON = response.data["spot"] as? JSON, let spot = try? Spot(spotJSON) {
                self.spot.clone(with: spot)
                self.pagination = nil
                self.getComments()
                self.loadData()
                self.tableView.reloadData()
            }
        }
    }
    
    func deleteSpot() {
        guard let id = spot.id else { return }
        APIClient().request(request: .DeleteSpot(vc: self, id: id)) { [weak self] (response, success) in
            if success, let `self` = self {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func addToFavorites() {
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spot.id else { return }
        
        APIClient().request(request: .AddToFavorites(vc: self, id: spotId)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            if success {
                var favorite = !self.spot.favorite
                if let fav = response.data["favorite"] as? Bool {
                    favorite = fav
                }
                self.spot.favorite = favorite
                self.setFavItem()
            }
            self.isLoadingFav = false
        }
    }
    
    func getComments() {
        guard let spotId = self.spot.id else { return }
        var page = 1
        
        if let pagination = pagination {
            page = pagination.currentPage + 1
        }
        
        self.startLoading()
        APIClient().request(request: .SpotComments(vc: self, id: spotId, page: page)) { [weak self] (response, success) in
            self?.stopLoading()
            if success, let `self` = self {
                guard let commentsData = response.data["comments"] as? [JSON],
                    let comments = try? commentsData.compactMap(Comment.init).sorted(by: {$0.id < $1.id}),
                        let paginationData = response.data["pagination"] as? JSON,
                        let  pagination = try? Pagination(paginationData) else { self.showServerBadErrorResponse(); return }
                
                self.pagination = pagination
                
                if self.pagination?.currentPage ?? 1 == 1 {
                    self.spot.comments = []
                }
                
                self.spot.comments.append(contentsOf: comments)
                self.tableView.reloadData()
            }
        }
    }
    
    func animateCommentLoaderBtn(showLoader: Bool = false) {
        if showLoader {
            commentTxt.endEditing(true)
            self.commentLoader.startAnimating()
        } else {
            self.commentLoader.stopAnimating()
        }
        
        UIView.animate(withDuration: 0.4) {
            self.sendCommentBtn.visibilityWithAlpha(isHidden: showLoader)
            self.commentLoader.visibilityWithAlpha(isHidden: !showLoader)
        }
    }
    
    func send(comment: String) {
        if #available(iOS 10.0, *) {
            UIImpactFeedbackGenerator().impactOccurred()
        }
        if !AppDelegate.checkUser(self) { return }
        guard let spotId = self.spot.id else { return }
        self.animateCommentLoaderBtn(showLoader: true)
        let params: JSON = [
            "comment": comment
        ]
        
        APIClient().request(request: .AddComment(vc: self, id: spotId, params: params)) { [weak self] (response, success) in
            guard let `self` = self else { return }
            self.animateCommentLoaderBtn()
            if success {
                self.clear(self.commentTxt)
                self.spot.commentsCount = self.spot.commentsCount ?? 0 + 1
                
                guard let commentJSON = response.data["comment"] as? JSON, let comment = try? Comment(commentJSON) else { self.showServerBadErrorResponse(); return }
                if let pagination = self.pagination {
                    if pagination.currentPage == pagination.lastPage {
                        self.spot.comments.append(comment)
                    }
                } else {
                    self.spot.comments.append(comment)
                }
                self.tableView.reloadData()
                self.tableView.scrollToBottom()
            }
        }
    }
    
    func deleteComment(at index: Int) {
        let comment = spot.comments[index]
        let user = comment.user
        guard user.id == User.getFromDefaults()?.id ?? -1 else { return }
        APIClient().request(request: .DeleteComment(vc: self, id: comment.id)) { [weak self] (response, success) in
            if success, let `self` = self {
                self.spot.comments.remove(at: index)
                self.tableView.reloadData()
            }
        }
    }
    
    func checkIn() {
        guard AppDelegate.checkUser(self), let spotId = spot.id else { return }
        APIClient().request(request: .CheckIn(vc: self, id: spotId)) { (_, _) in }
    }
}
