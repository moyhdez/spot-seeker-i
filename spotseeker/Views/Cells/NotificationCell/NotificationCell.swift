//
//  NotificationCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var summaryLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var seenView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.setCircular()
        imgView.lightBorder()
        seenView.setCircular()
    }
    
    func set(for notification: NotificationLocal) {
        imgView.af_setImage(withURL: notification.data.image)
        titleLbl.text = notification.data.title
        summaryLbl.text = notification.data.content
        dateLbl.text = notification.createdAt
        //TODO: HANDLE THIS
        seenView.visibilityWithAlpha(isHidden: true)
    }
    
}
