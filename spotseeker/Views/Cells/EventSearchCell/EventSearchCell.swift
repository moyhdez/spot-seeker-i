//
//  EventSearchCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class EventSearchCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func set(for event: Event) {
        if let image = event.image {
            self.imgView.af_setImage(withURL: image)
        }
    }
    
}
