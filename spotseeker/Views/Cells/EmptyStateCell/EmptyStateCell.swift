//
//  EmptyStateCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 11/11/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class EmptyStateCell: UITableViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLbl.text = NSLocalizedString("No results found", comment: "")
    }
    
}
