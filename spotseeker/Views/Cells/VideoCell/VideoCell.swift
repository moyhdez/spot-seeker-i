//
//  VideoCell.swift
//  spotfinder
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(for urlString: String) {
        imgView.image = UIImage.createThumbnailOfVideoFromRemoteUrl(url: urlString)
    }

}
