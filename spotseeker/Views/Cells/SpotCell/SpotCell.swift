//
//  SpotCell
//  spotseeker
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit
import CoreLocation
import ImageSlideshow

class SpotCell: UITableViewCell {
    
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotDescription: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var distanceBtn: UIButton!
    @IBOutlet weak var userBtn: UIButton!
    @IBOutlet weak var difficultyStack: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userIV.setCircular()
        userIV.lightBorder()
        slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
        slideshow.contentScaleMode = .scaleAspectFill
    }
    
    
    func setUpWith(spot: Spot, location: CLLocationCoordinate2D? = nil) {
        spotDescription.text = spot.summary
        spotTitle.text = spot.title
        userName.text = spot.user?.name
        
        for (i, view) in difficultyStack.arrangedSubviews.enumerated() {
            if let imgView = view as? UIImageView {
                imgView.image = spot.difficulty ?? 5 > i ? #imageLiteral(resourceName: "wheel active") : #imageLiteral(resourceName: "wheel")
            }
        }
        
        if let location = location {
            distanceLbl.text = spot.coordinates()?.readableDistance(to: location)
        } else {
            distanceLbl.text = NSLocalizedString("Go", comment: "")
        }
        
        topBtn.setImage(spot.favorite ? #imageLiteral(resourceName: "fav_active") : #imageLiteral(resourceName: "fav"), for: .normal)
        
        let sources = spot.images.compactMap { AlamofireSource(urlString: $0) }
        slideshow.setImageInputs(sources)
        
        if let image = spot.user?.image {
            userIV.downloadWithLoader(url: image, color: UIColor.lightGray, largeLoader: false)
        }
    }

}
