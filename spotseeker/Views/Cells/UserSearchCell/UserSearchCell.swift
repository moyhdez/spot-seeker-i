//
//  UserSearchCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class UserSearchCell: UITableViewCell {
    @IBOutlet weak var userIV: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var addUserBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        userIV.setCircular()
        userIV.lightBorder()
    }
    
    func set(for user: User) {
        userIV.af_setImage(withURL: user.image)
        userName.text = user.name
        addUserBtn.visibilityWithAlpha(isHidden: user.id == User.getFromDefaults()?.id ?? -1)
        addUserBtn.setImage(user.following ? #imageLiteral(resourceName: "follower") : #imageLiteral(resourceName: "adduser"), for: .normal)
    }
    
}
