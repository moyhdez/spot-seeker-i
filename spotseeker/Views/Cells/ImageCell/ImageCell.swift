//
//  ImageCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var selectImageView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 1
        self.selectImageView.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1).cgColor
        self.selectImageView.layer.borderWidth = 1
    }
}
