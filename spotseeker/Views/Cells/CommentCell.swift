//
//  CommentCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 29/08/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var userBtn: UIButton!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var optionsBtn: UIButton!
    
    func setUp(for comment: Comment) {
        dateLbl.text = comment.date
        commentLbl.text = comment.comment
        userNameLbl.text = comment.user.name
        optionsBtn.isHidden = comment.user.id != User.getFromDefaults()?.id ?? -1
        userImg.setCircular()
        userImg.lightBorder()
        userImg.downloadWithLoader(url: comment.user.image, color: UIColor.lightGray, largeLoader: false)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
