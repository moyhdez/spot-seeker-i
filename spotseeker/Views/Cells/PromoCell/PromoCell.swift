//
//  PromoCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class PromoCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.borderColor = #colorLiteral(red: 0.9372549057, green: 0.9372549057, blue: 0.9568627477, alpha: 1).cgColor
        imgView.layer.borderWidth = 1
    }
    
    func set(for promo: Promotion) {
        self.titleLbl.text = promo.name
        self.priceLbl.text = promo.price
        if let image = promo.image {
            imgView.af_setImage(withURL: image)
        }
    }

}
