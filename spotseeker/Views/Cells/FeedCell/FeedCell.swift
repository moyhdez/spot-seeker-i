//
//  FeedCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 17/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var summaryLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(for post: FeedPost) {
        dateLbl.text = post.updatedAt
        titleLbl.text = post.title
        summaryLbl.text = post.content
        imgView.af_setImage(withURL: post.image)
    }
    
}
