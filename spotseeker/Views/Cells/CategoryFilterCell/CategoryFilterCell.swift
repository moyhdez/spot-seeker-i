//
//  CategoryFilterCell.swift
//  spotseeker
//
//  Created by Moy Hdez on 16/09/18.
//  Copyright © 2018 Moy Hdez. All rights reserved.
//

import UIKit

class CategoryFilterCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var imgSelected: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(for category: Category, selected: Bool) {
        self.titleLbl.text = category.title
        imgSelected.image = selected ? #imageLiteral(resourceName: "cb_checked") : #imageLiteral(resourceName: "cb_unchecked")
    }
    
}
