//
//  Helper.swift
//  spotseeker
//
//  Created by Moy Hdez on 25/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation
import AlamofireImage
import Alamofire

extension Date {
    func toStringWith(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_MX")
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}

extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}
