//
//  Constants.swift
//  spotseeker
//
//  Created by Moy Hdez on 10/07/17.
//  Copyright © 2017 Moy Hdez. All rights reserved.
//

import Foundation

struct K {
    struct Server {
        static let BaseURL = "http://spotseeker.cjapps.com.mx/"
        static let clientId = "2"
        static let clientSecret = "jECFbiRWznmw8ORpG4qZcBpLwpTjKe1Sym12q3Fx"
    }
    
    struct Defaults {
        static let DateFormat = "yyyy-MM-dd' 'HH:mm:ss"
        static let User = "user"
        static let Locale = "Locale"
        static let DeviceToken = "device_token"
        static let AccessToken = "access_token"
        static let RefreshToken = "refresh_token"
        static let TokenExpiresAt = "token_expires_at"
    }
}
